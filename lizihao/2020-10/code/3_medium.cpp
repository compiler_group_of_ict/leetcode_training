// 3. 无重复字符的最长子串
// https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/


class Solution {
    int pos[256];
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.size();
        if(n < 1)
            return 0;
        
        for(int i = 0; i < 256; i++)
            pos[i] = -1;
        
        int start = 0;
        int retval = 0;
        // window : s[start : i], check if it is no dupilicate
        for(int i = 0; i < n; i++){
            if(pos[s[i]] >= start){
                retval = std::max(retval, i - start);
                start = pos[s[i]] + 1;
            }
            pos[s[i]] = i;
        }
        retval = std::max(retval, n - start);
        return retval;
    }
};
