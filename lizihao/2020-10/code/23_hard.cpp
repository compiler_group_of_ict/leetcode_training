// 23. 合并K个升序链表
// https://leetcode-cn.com/problems/merge-k-sorted-lists/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */


ListNode* mergeTwoLists(ListNode* head1, ListNode* head2){
    if(head2 == nullptr)
        return head1;
    if(head1 == nullptr)
        return head2;
    
    ListNode* dummy_head = new ListNode(0);
    ListNode* tail = dummy_head;

    while(head1 || head2){
        if(head1 == nullptr){
            tail->next = head2;
            tail = head2;
            head2 = head2->next;
        } else if(head2 == nullptr || head1->val < head2->val){
            tail->next = head1;
            tail = head1;
            head1 = head1->next;
        } else {
            tail->next = head2;
            tail = head2;
            head2 = head2->next;
        }
    }

    ListNode* retval = dummy_head->next;
    delete dummy_head;
    return retval;
}

ListNode* mergeKListsDivideAndConquer(vector<ListNode*>& lists, int i, int j){
    if(i == j)
        return lists[i];    
    int mid = (i + j) / 2;
    ListNode * left = mergeKListsDivideAndConquer(lists, i, mid);
    ListNode * right = mergeKListsDivideAndConquer(lists, mid + 1, j);
    return mergeTwoLists(left, right);
}


class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int n = lists.size();
        if(n == 0)
            return nullptr;
        else if(n == 1)
            return lists[0];
        else
            return mergeKListsDivideAndConquer(lists, 0, n - 1);
    }
};
