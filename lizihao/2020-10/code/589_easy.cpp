// 589. N叉树的前序遍历
// https://leetcode-cn.com/problems/n-ary-tree-preorder-traversal/

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/


class Solution {
public:

    void preorderTraversalIter(Node* root, vector<int> & result) {
        if(!root)
            return;
        result.push_back(root->val);
        for(auto child : root->children)
            preorderTraversalIter(child, result);
    }

    vector<int> preorder(Node* root) {
        vector<int> result;
        preorderTraversalIter(root, result);
        return std::move(result);
    }
};
