// 442. 数组中重复的数据
// https://leetcode-cn.com/problems/find-all-duplicates-in-an-array/

class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {
        vector<int> retval;
        nums.push_back(0);
        nums.back() = nums[0];

        int n = nums.size();
        for(int i = 1; i < n; i++){
            while(nums[i] != i && nums[i] != nums[nums[i]]){
                std::swap(nums[i], nums[nums[i]]);
            }
        }

        for(int i = 1; i < n; i++){
            if(nums[i] != i)
                retval.push_back(nums[i]);
        }
        
        nums[0] = nums.back();
        nums.pop_back();
        return std::move(retval);
    }
};
