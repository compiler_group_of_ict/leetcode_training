// 155. 最小栈
// https://leetcode-cn.com/problems/min-stack/

class MinStack {
    std::vector<std::pair<int, int>> data; // <value, current-min>
public:
    /** initialize your data structure here. */
    MinStack() { }
    
    void push(int x) {
        auto min_val = data.empty() ? x : std::min(data.back().second, x);
        data.push_back(make_pair(x, min_val));
    }
    
    void pop() {
        data.pop_back();
    }
    
    int top() {
        return data.back().first;
    }
    
    int getMin() {
        return data.back().second;
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
