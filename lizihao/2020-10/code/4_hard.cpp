// 4. 寻找两个正序数组的中位数
// https://leetcode-cn.com/problems/median-of-two-sorted-arrays/

double findMedianSingleSortedArrays(const vector<int>& nums){
    int n = nums.size();

    if(1 == (n & 1))
        return nums[(n - 1) / 2];
    else 
        return (nums[n / 2 - 1] + nums[n / 2]) / 2.0f;
}

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int n1 = nums1.size();
        int n2 = nums2.size();
        if(n1 == 0)
            return findMedianSingleSortedArrays(nums2);
        if(n2 == 0)
            return findMedianSingleSortedArrays(nums1);
        
        // Status: nums1 and nums2 are both non-empty
        vector<int> nums3(n1 + n2);
        std::merge(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), nums3.begin());
        return findMedianSingleSortedArrays(nums3);
    }
};
