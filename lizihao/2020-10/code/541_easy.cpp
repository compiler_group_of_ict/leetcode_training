// 541. 反转字符串 II
// https://leetcode-cn.com/problems/reverse-string-ii/


void reverseString(string & str, int i, int j){
    int mid = (i + j - 1) >> 1;
    int sum = i + j;
    while(i <= mid){
        std::swap(str[i], str[sum - i - 1]);
        i++;
    }
}

class Solution {
public:
    string reverseStr(string s, int k) {
        int n = s.size();
        for(int i = 0; i < n; i += (k << 1)){
            reverseString(s, i, std::min(n, i + k));
        }
        return s;
    }
};

