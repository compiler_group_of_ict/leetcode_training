// 剑指 Offer 11. 旋转数组的最小数字
// https://leetcode-cn.com/problems/xuan-zhuan-shu-zu-de-zui-xiao-shu-zi-lcof/


class Solution {
public:

    int findMinIter(vector<int>& nums, int left, int right){
        if(left == right){
            return nums[left];
        } else if(left + 1 == right){
            return std::min(nums[left], nums[right]);
        } else {
            int mid = (left + right)/ 2;
            if(nums[mid] > nums[right]){
                return findMinIter(nums, mid + 1, right);
            }
            else if(nums[mid] < nums[right]){
                return findMinIter(nums, left, mid);
            } else {
                return findMinIter(nums, left, right-1);
                //return std::min(findMinIter(nums, left, mid), findMinIter(nums, mid + 1, right));
            }
        }
    }

    int minArray(vector<int>& numbers) {
        return findMinIter(numbers, 0, numbers.size() - 1);
    }
};
