// 8. 字符串转换整数 (atoi)
// https://leetcode-cn.com/problems/string-to-integer-atoi/


class Solution {
public:
    int myAtoi(string s) {
        long result = 0;
        int is_negative = 0;

        auto iter = s.begin();
        while(iter != s.end() && isblank(*iter))
            iter++;
        
        if(iter != s.end()){
            if(*iter == '-'){
                is_negative = 1;
                iter++;
            } else if(*iter == '+'){
                iter++;
            } else if(!isdigit(*iter)){
                return 0;
            }
            
            long max_integer = long(INT_MAX) + is_negative;
            // there is no sign before current character
            while(iter != s.end() && isdigit(*iter)){
                result = result * 10 + (*iter - '0');
                if(result > max_integer){
                    return is_negative ? INT_MIN : INT_MAX;
                }
                iter++;
            }
        }
        
        return is_negative ? -result : result;;
    }
};
