// 1207. 独一无二的出现次数
// https://leetcode-cn.com/problems/unique-number-of-occurrences/

class Solution {
    enum {SIZE = 2001};
    int count[SIZE];
public:
    inline void countOccurrences(const vector<int>& arr){
        for(auto i = 0; i < SIZE; i++){
            count[i] = 0;
        }

        for(const auto & x : arr){
            ++count[x + 1000];
        }
    }

    bool uniqueOccurrences(vector<int>& arr) {
        countOccurrences(arr);

        std::unordered_set<int> occurrences;
        for(const auto & c : count){
            if(c != 0){
                if(occurrences.find(c) == occurrences.end()){
                    occurrences.insert(c);
                } else{
                    return false;
                }
            }
        }

        return true;
    }
};
