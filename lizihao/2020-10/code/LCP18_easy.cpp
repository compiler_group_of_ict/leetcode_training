// LCP 18. 早餐组合
// https://leetcode-cn.com/problems/2vYnGI/

class Solution {
public:
    int countLessThanX(vector<int>& v,int x){
        if(v.back() <= x) 
            return v.size();

        int l = 0,r = v.size()-1;
        while(l < r){
            int mid = l+(r-l)/2;
            if(v[mid] <= x) 
                l = mid+1;
            else 
                r = mid;
        }
        return l;
    }
    
    int breakfastNumber(vector<int>& staple, vector<int>& drinks, int x) {
        int res = 0;
        if(staple.size() <= drinks.size()){
            sort(staple.begin(),staple.end());
            for(int n: drinks) 
                res = (res+countLessThanX(staple,x-n))%1000000007;
        }else{
            sort(drinks.begin(),drinks.end());
            for(int n: staple)
                res = (res+countLessThanX(drinks,x-n))%1000000007;
        }return res;
    }
};
