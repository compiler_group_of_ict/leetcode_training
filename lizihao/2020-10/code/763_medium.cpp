// 763. 划分字母区间
// https://leetcode-cn.com/problems/partition-labels/

class Solution {
    int last[26];
public:
    vector<int> partitionLabels(string S) {
        int n = S.size();
        for(int i = 0; i < n; i++){
            last[S[i] - 'a'] = i;
        }

        std::vector<int> result;

        int start = 0;
        int end = 0;

        for(int i = 0; i < n; i++){
            end = std::max(end, last[S[i] - 'a']);
            if(i == end){
                result.push_back(end - start + 1);
                start = i + 1;
            }
        }

        return result;
    }
};

