// 2. 两数相加
// https://leetcode-cn.com/problems/add-two-numbers/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* dummy_head = new ListNode(0);
        ListNode* tail = dummy_head;

        int carry = 0;
        while(l1 || l2){
            int val1 = 0;
            int val2 = 0;
            if(l1){
                val1 = l1->val;
                l1 = l1->next;
            }
            if(l2){
                val2 = l2->val;
                l2 = l2->next;
            }
            int sum = val1 + val2 + carry;
            carry = sum / 10;
            
            tail->next = new ListNode(sum % 10);
            tail = tail->next;
        }

        if(carry != 0){
            tail->next = new ListNode(1);
        }
        ListNode* retval = dummy_head->next;
        delete dummy_head;
        return retval;
    }
};
