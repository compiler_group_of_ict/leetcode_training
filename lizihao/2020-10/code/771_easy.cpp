// 771. 宝石与石头
// https://leetcode-cn.com/problems/jewels-and-stones/

class Solution {
    int tbl[52];
public:
    int numJewelsInStones(string J, string S) {
        for(int i = 0; i < 52; i++)
            tbl[i] = 0;

        for(auto ch : S){
            if(ch >= 'a' && ch <= 'z')
                tbl[ch - 'a'] += 1;
            else if(ch >= 'A' && ch <= 'Z')
                tbl[ch - 'A' + 26] += 1;
        }

        int result = 0;
        for(auto ch : J){
            if(ch >= 'a' && ch <= 'z')
                result += tbl[ch - 'a'];
            else if(ch >= 'A' && ch <= 'Z')
                result += tbl[ch - 'A' + 26];
        }
        
        return result;
    }
};
