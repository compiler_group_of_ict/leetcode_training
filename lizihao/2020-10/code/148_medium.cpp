// 148. 排序链表
// https://leetcode-cn.com/problems/sort-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

int getLength(ListNode * head){
    int result = 0;
    while(head){
        ++result;
        head = head->next;
    }
    return result;
}

// Input Constarint: 0 <= idx < Length(head)
ListNode * getElement(ListNode * head, int idx){
    while(idx > 0){
        head = head->next;
        idx -= 1;
    }
    return head;
}

// Input Constarint: 0 <= idx < Length(head)-1
ListNode * splitAfter(ListNode * head, int idx){
    head = getElement(head, idx);
    auto tmp = head->next;
    head->next = nullptr;
    return tmp;
}

ListNode * merge(ListNode * left, ListNode * right){
    ListNode * dummy_head = new ListNode(0);
    ListNode * tail = dummy_head;
    
    while(left || right){
        if(left == nullptr){
            tail->next = right;
            tail = right;
            right = right->next;
        } else if(right == nullptr || (left->val < right->val)){
            tail->next = left;
            tail = left;
            left = left->next;
        } else {
            tail->next = right;
            tail = right;
            right = right->next;
        }
    }
    auto result = dummy_head->next;
    delete dummy_head;
    return result;
}

ListNode * merge_sort(ListNode * head, int n){
    if(n <= 1)
        return head;
    
    ListNode * right = merge_sort(splitAfter(head, (n - 1)/2), n/2);
    ListNode * left  = merge_sort(head, (n + 1) / 2);
    return merge(left, right);
}

class Solution {
public:
    ListNode* sortList(ListNode* head) {
        if(!head || !(head->next))
            return head;

        // Status : there are at least 2 elements in the single linked list
        return merge_sort(head, getLength(head));
    }
};
