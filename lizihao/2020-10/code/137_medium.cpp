// 137. 只出现一次的数字 II
// https://leetcode-cn.com/problems/single-number-ii/

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ones = 0;
        int twos = 0;
        for(const auto & x : nums){
            ones = ones ^ x & ~twos;
            twos = twos ^ x & ~ones;
        }
        return ones;
    }
};
