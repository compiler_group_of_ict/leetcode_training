// 1. 两数之和
// https://leetcode-cn.com/problems/two-sum/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int,int> tbl; // Map(val->idx)
        int n = nums.size();
        vector<int> result;
        for(int i = 0; i < n; i++){
            auto iter = tbl.find(target - nums[i]);
            if(iter != tbl.end()){
                result.push_back(iter->second);
                result.push_back(i);
                break;
            }
            tbl.insert(make_pair(nums[i], i));
        }
        return result;
    }
};
