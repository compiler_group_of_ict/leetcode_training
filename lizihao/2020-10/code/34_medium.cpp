// 34. 在排序数组中查找元素的第一个和最后一个位置
// https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/

class Solution {
public:

    int findFirst(vector<int>& nums, int target, int l, int r) {
        if(l > r){
            return -1;
        }else if(l == r){
            return nums[l] == target ? l : -1;
        } else if (l + 1 == r){
            if(nums[l] == target)
                return l;
            else if(nums[r] == target)
                return r;
            else
                return -1;
        } else{
            int mid = (l + r) / 2;
            if(nums[mid] < target)
                return findFirst(nums, target, mid + 1, r);
            else 
                return findFirst(nums, target, l, mid);
        }
    }

    int findLast(vector<int>& nums, int target, int l, int r) {
        if(l > r){
            return -1;
        }else if(l == r){
            return nums[l] == target ? l : -1;
        } else if (l + 1 == r){
            if(nums[r] == target)
                return r;
            else if(nums[l] == target)
                return l;
            else
                return -1;
        } else{
            int mid = (l + r) / 2;
            if(nums[mid] > target)
                return findLast(nums, target, l, mid - 1);
            else 
                return findLast(nums, target, mid, r);
        }
    }


    vector<int> searchRange(vector<int>& nums, int target) {
        int l = 0;
        int r = nums.size() - 1;
        vector<int> result;
        
        result.push_back(findFirst(nums, target, l, r));
        result.push_back(findLast(nums, target, l, r));
        
        return result;
    }
};
