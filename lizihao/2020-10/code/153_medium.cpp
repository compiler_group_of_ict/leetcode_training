// 153. 寻找旋转排序数组中的最小值
// https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array/

class Solution {
public:

    /*int findMinIter(vector<int>& nums, int left, int right){
        if(left == right){
            return nums[left];
        } else if(left + 1 == right){
            return std::min(nums[left], nums[right]);
        } else {
            int mid = (left + right)/ 2;
            if(nums[mid] > nums[right]){
                return findMinIter(nums, mid + 1, right);
            }
            else{
                return findMinIter(nums, left, mid);
            }
        }
    }*/

    int findMin(vector<int>& nums) {
        int l = 0;
        int r = nums.size() - 1;

        while(l < r){
            int mid = (l + r)/ 2;
            if(nums[mid] > nums[r]){
                l = mid + 1;
            }else{
                r = mid;
            }
        }

        return nums[l];

        //return findMinIter(nums, 0, nums.size() - 1);
    }
};
