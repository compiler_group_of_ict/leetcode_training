// 516. 最长回文子序列
// https://leetcode-cn.com/problems/longest-palindromic-subsequence/ 

class Solution {
    int dp[1000][1000];
public:
    int longestPalindromeSubseq(string s) {
        int n = s.size();
        if(n < 2)
            return n;

        for(int i = 0; i < n; i++){
            dp[i][i] = 1;
            for(int j = i + 2; j < n; j++)
                dp[i][j] = 0;
        }

        for(int i = 1; i < n; i++){
            dp[i - 1][i] = (s[i - 1] == s[i]) ? 2 : 1;
        }

        for(int len = 3; len <= n; len++){
            for(int last = len - 1; last < n; last++){
                int start = last - len + 1;
                dp[start][last] = (s[start] == s[last]) ? 2 + dp[start + 1][last - 1] :
                                  std::max(dp[start + 1][last], dp[start][last - 1]);
            }
        }
        return dp[0][n - 1];        
    }
};
