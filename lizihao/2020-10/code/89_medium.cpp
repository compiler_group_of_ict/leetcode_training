// 89. 格雷编码
// https://leetcode-cn.com/problems/gray-code/



class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> retval;
        retval.push_back(0);
        for(int i = 0; i < n; i++){
            int t = 1 << i;
            for(int j = t - 1; j >= 0; j--){
                retval.push_back(retval[j] | (1 << i));
            }
        }
        return retval;
    }
};
