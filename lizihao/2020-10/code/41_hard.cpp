// 41. 缺失的第一个正数
// https://leetcode-cn.com/problems/first-missing-positive/

class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        nums.push_back(0);
        std::swap(nums[0], nums.back());

        int n = nums.size();
        for(int i = 1; i < n; i++){
            while(nums[i] > 0 && nums[i] < n && nums[i] != i){
                if(nums[i] == nums[nums[i]]){
                    nums[nums[i]] = nums[i];
                    break;
                } else{
                    std::swap(nums[i], nums[nums[i]]);
                }
                // Status: nums[nums[i]] = nums[i]
            }
        }

        int retval = n;
        for(int i = 1; i < n; i++)
            if(nums[i] != i){
                retval = i;
                break;
            }

        nums[0] = nums.back();
        nums.pop_back();
        return retval;
    }
};
