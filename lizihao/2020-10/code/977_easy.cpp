// 977. 有序数组的平方
// https://leetcode-cn.com/problems/squares-of-a-sorted-array/

class Solution {
public:
    vector<int> sortedSquares(vector<int>& A) {
        for(auto & a : A){
            a = a * a;
        }

        std::sort(A.begin(), A.end());
        return std::move(A);
    }
};
