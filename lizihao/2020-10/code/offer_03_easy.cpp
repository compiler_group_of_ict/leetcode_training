// 剑指 Offer 03. 数组中重复的数字
// https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/

class Solution {
public:
    int findRepeatNumber(vector<int>& nums) {
        int n = nums.size();
        for(int i = 0; i < n; i++){
            while(nums[i] != i){
                if(nums[nums[i]] == nums[i])
                    return nums[i];
                std::swap(nums[i], nums[nums[i]]);
            }
        }
        return nums[0];
    }
};
