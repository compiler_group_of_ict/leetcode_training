
// 429.N叉树的层序遍历
//https://leetcode-cn.com/problems/n-ary-tree-level-order-traversal/

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    void levelOrderIter(Node* root, int level, vector<vector<int>> & result){
        if(result.size() == level){
            result.push_back(vector<int>());
        }
        result[level].push_back(root->val);

        for(const auto & child : root->children){
            if(!child) continue;

            levelOrderIter(child, level + 1, result);
        }
    }

    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> result;
        if(root) levelOrderIter(root, 0, result);
        return std::move(result);
    }
};
