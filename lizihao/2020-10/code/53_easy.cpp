// 53. 最大子序和
// https://leetcode-cn.com/problems/maximum-subarray/

class Solution {
    int maxSubArrayHelper(vector<int>& nums, int left, int right){
        if(left == right){
            return nums[left];
        }

        int mid = (left + right) / 2;
        int result = maxSubArrayHelper(nums, left, mid);
        result = std::max(result, maxSubArrayHelper(nums, mid+1, right));

        int left_max = INT_MIN;
        for(int i = mid, sum = 0; i >= left; i--){
            sum += nums[i];
            left_max = std::max(sum, left_max);
        }

        int right_max = INT_MIN;
        for(int i = mid + 1, sum = 0; i <= right; i++){
            sum += nums[i];
            right_max = std::max(sum, right_max);
        }

        result = std::max(result, left_max + right_max);
        return result;

    }
public:
    int maxSubArray(vector<int>& nums) {
        return maxSubArrayHelper(nums, 0, nums.size() - 1);
    }
};
