// 154. 寻找旋转排序数组中的最小值 II
// https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array-ii/

class Solution {
public:

    int findMinIter(vector<int>& nums, int left, int right){
        if(left == right){
            return nums[left];
        } else if(left + 1 == right){
            return std::min(nums[left], nums[right]);
        } else {
            int mid = (left + right)/ 2;
            if(nums[mid] > nums[right]){
                return findMinIter(nums, mid + 1, right);
            }
            else if(nums[mid] < nums[right]){
                return findMinIter(nums, left, mid);
            } else {
                //return findMinIter(nums, left, right-1);
                return std::min(findMinIter(nums, left, mid), findMinIter(nums, mid + 1, right));
            }
        }
    }

    int findMin(vector<int>& nums) {
        return findMinIter(nums, 0, nums.size() - 1);
    }
};

