// 371. 两整数之和
// https://leetcode-cn.com/problems/sum-of-two-integers/


class Solution {
public:
    int getSum(int a, int b) {
        unsigned result = a;
        unsigned carry = b;

        while(carry != 0){
            int old_result = result;
	    // each bits' result
            result = result ^ carry;
            // each bits' carry
            carry = (old_result & carry) << 1; 
	    // Claim: the number of "1" which "carry" contains must decresase, so the loop can finally terminate
        }
        return (int) result;
    }
};
