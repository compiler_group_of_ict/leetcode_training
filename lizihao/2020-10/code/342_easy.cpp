// 342. 4的幂
// https://leetcode-cn.com/problems/power-of-four/

class Solution {
public:
    bool isPowerOfFour(int num) {
        return num > 0 && (num & (num - 1)) == 0 && (num & 0x55555555U) != 0;
    }
};
