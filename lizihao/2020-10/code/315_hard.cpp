// 315. 计算右侧小于当前元素的个数
// https://leetcode-cn.com/problems/count-of-smaller-numbers-after-self/


class Solution {
    inline int parent(int idx){
        return idx - (idx & (-idx));
    }

    inline int sibling(int idx){
        return idx + (idx & (-idx));
    }

    int query(const vector<int> &bit, int idx){
        idx += 1;
        int result = 0;
        while(idx != 0){
            result += bit[idx];
            idx = parent(idx);
        }
        return result;
    }

    void addOne(vector<int> &bit, int idx){
        idx += 1;
        int n = bit.size();
        while(idx < n){
            bit[idx] += 1;
            idx = sibling(idx);
        }
    }

public:
    vector<int> countSmaller(vector<int>& nums) {
        // 离散化
        vector<int> tmp(nums);
        std::sort(tmp.begin(), tmp.end());
        for(auto & num : nums){
            num = std::lower_bound(tmp.begin(), tmp.end(), num) - tmp.begin();
        }

        int n = nums.size();
        vector<int> bit(n + 1, 0);
        vector<int> retval(n);
        for(int i = n - 1; i >= 0; i--){
            retval[i] = query(bit, nums[i] - 1);
            addOne(bit, nums[i]);
        }
        return retval;
    }
};
