// 59. 螺旋矩阵 II
// https://leetcode-cn.com/problems/spiral-matrix-ii/



class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> retval(n);
        for(int i = 0; i < n; i++)
            retval[i].resize(n);
        
        int count = 0;
        for(int i = 0; i < n; i++){
            for(int t = i; t < n - i; t++)
                retval[i][t] = ++count;

            for(int t = i + 1; t < n - i; t++)
                retval[t][n - i - 1] = ++count;

            for(int t = n - i - 2; t >= i; t--)
                retval[n - i - 1][t] = ++count;

            for(int t = n - i - 2; t > i; t--)
                retval[t][i] = ++count;
        }

        return std::move(retval);
    }
};
