// 912. 排序数组
// https://leetcode-cn.com/problems/sort-an-array/

// stl internal implementaion

void insertion_sort(vector<int>& nums, int i, int j){
    for(int m = i + 1; m <= j; m++){
        int t = m - 1;
        int val = nums[m];
        while(t >= i && val < nums[t]){
            nums[t + 1] = nums[t];
            t -= 1;
        }
        nums[t + 1] = val;
    }
}

void heap_sort(vector<int>& nums, int first, int last){
    auto begin = nums.begin() + first;
    auto end = nums.begin() + last + 1;
    std::make_heap(begin, end);

    for(int i = last + 1; i > first; i--){
        std::pop_heap(nums.begin() + first, nums.begin() + i);
    }
}

void quicksort(vector<int>& nums, int first, int last, int recur_lev){
    if(last - first + 1 <= 15){
        insertion_sort(nums, first, last);
        return;
    } else if(recur_lev <= 0){
        heap_sort(nums, first, last);
        return;
    }

    int pivot = nums[last];
    int t = first - 1;
    for(int k = first; k < last; k++){
        if(nums[k] < pivot){
            std::swap(nums[k], nums[++t]);
        }
    }
    std::swap(nums[last], nums[++t]);

    quicksort(nums, first, t - 1, recur_lev - 1);
    quicksort(nums, t + 1, last, recur_lev - 1);
}

class Solution {
public:
    vector<int> sortArray(vector<int>& nums) {
        const int n = nums.size();
        if(n >= 2)
            quicksort(nums, 0, n - 1, std::log2(n));
        return nums;
    }
};
