// 剑指 Offer 64. 求1+2+…+n
// https://leetcode-cn.com/problems/qiu-12n-lcof/


class Solution {
public:
    int sumNums(int n) {
        if(n % 2 == 0)
            return n/2 * (n + 1);
        else
            return (n + 1)/2 * n;
    }
};
