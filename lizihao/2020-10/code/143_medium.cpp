// 143. 重排链表
// https://leetcode-cn.com/problems/reorder-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* findFirstHalfEnd(ListNode* head){
        ListNode* fast = head;
        ListNode* slow = head;
        while(fast->next && fast->next->next){
            slow = slow->next;
            fast = fast->next->next;
        }
        return slow;
    }

    ListNode* reverseList(ListNode* head){
        ListNode* dummy = new ListNode();
        while(head){
            auto next = head->next;
            head->next = dummy->next;
            dummy->next = head;
            head = next;
        }
        head = dummy->next;
        delete dummy;
        return head;
    }


    void reorderList(ListNode* head) {
        if(!head)
            return;
            
        ListNode* firstHalfEnd =findFirstHalfEnd(head);
        ListNode* secondHalfBebin = reverseList(firstHalfEnd->next);
        firstHalfEnd->next = nullptr;
        
        ListNode* after = head;
        ListNode* firstHalfBebin = head->next;
        
        while(firstHalfBebin || secondHalfBebin){
            after->next = secondHalfBebin;
            after = secondHalfBebin;
            secondHalfBebin = secondHalfBebin->next;

            if(firstHalfBebin)
            {
                after->next = firstHalfBebin;
                after = firstHalfBebin;
                firstHalfBebin = firstHalfBebin->next;
            }
        }
    }
};
