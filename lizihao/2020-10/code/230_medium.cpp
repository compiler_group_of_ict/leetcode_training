// 230. 二叉搜索树中第K小的元素
// https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void kthSmallestIter(TreeNode* root, int k, vector<int> &buf) {
        if(!root)
            return;

        kthSmallestIter(root->left, k, buf);
        buf.push_back(root->val);
        if(buf.size() == k)
            return;
        kthSmallestIter(root->right, k, buf);
    }

    int kthSmallest(TreeNode* root, int k) {
        vector<int> buf;
        kthSmallestIter(root, k, buf);
        return buf[k - 1];
    }
};
