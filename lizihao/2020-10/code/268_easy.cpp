// 268. 缺失数字
// https://leetcode-cn.com/problems/missing-number/


class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int n = nums.size();
        int sum = ((n + 1) * n) >> 1;
        int acc = 0;
        for(auto num : nums)
            acc += num;
        return sum - acc;
    }
};
