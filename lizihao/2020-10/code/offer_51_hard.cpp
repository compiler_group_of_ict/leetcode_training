// 剑指 Offer 51. 数组中的逆序对
// https://leetcode-cn.com/problems/shu-zu-zhong-de-ni-xu-dui-lcof/

// Input Constraint: 0 <= first <= mid < last < nums.size() && length(swap_buf) == nums.size()
int reversePairsMerge(vector<int> & nums, size_t first, size_t mid, size_t last, int * swap_buf){
    std::copy(nums.begin() + first, nums.begin() + mid + 1, swap_buf + first);
    std::copy(nums.begin() + mid + 1, nums.begin() + last + 1, swap_buf + mid + 1);
    
    size_t dst = first;
    size_t src1 = first;
    size_t src2 = mid + 1;
    int result = 0;

    while(dst <= last){
        if(src1 > mid){
            nums[dst] = swap_buf[src2++];
        } else if(src2 > last || swap_buf[src1] <= swap_buf[src2]){
            nums[dst] = swap_buf[src1++];
        } else {
            // Status: src1 <= mid && src2 <= last && swap_buf[src1] > swap_buf[src2]
            result += mid + 1 - src1;
            nums[dst] = swap_buf[src2++];
        }
        ++dst;
    }

    return result;
}

// Input Constraint: 0 <= first <= last < nums.size() && length(swap_buf) == nums.size()
int reversePairsDivideAndConquer(vector<int> & nums, size_t first, size_t last, int * swap_buf){
    if(first == last)
        return 0;
    else if(first + 1 == last){
        if(nums[first] > nums[last]){
            std::swap(nums[first], nums[last]);
            return 1;
        } else {
            return 0;
        }
    }
    // STATUS : range length > 2
    size_t mid = (first + last) / 2;
    auto acc  = reversePairsDivideAndConquer(nums, first, mid, swap_buf);
    acc += reversePairsDivideAndConquer(nums, mid + 1, last, swap_buf);
    acc += reversePairsMerge(nums, first, mid, last, swap_buf);
    return acc;
}

class Solution {
public:
    int reversePairs(vector<int>& nums) {
        const size_t n = nums.size();
        if(n < 2)
            return 0;
        
        int * swap_buf = new int[n]; // serve as a buffer
        int result = reversePairsDivideAndConquer(nums, 0, n - 1, swap_buf);
        delete []swap_buf;
        return result;
    }
};
