// 876. 链表的中间结点
// https://leetcode-cn.com/problems/middle-of-the-linked-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        std::vector<ListNode*> buf;
        buf.push_back(head);
        while(auto p = buf.back()->next){
            buf.push_back(p);
        }
        return buf[buf.size()/2];
    }
};
