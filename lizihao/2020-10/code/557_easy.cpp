// 557. 反转字符串中的单词 III
// https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/


void reverseString(string & str, int i, int j){
    int mid = (i + j - 1) >> 1;
    int sum = i + j;
    while(i <= mid){
        std::swap(str[i], str[sum - i - 1]);
        i++;
    }
}

int findFirstChar(string & str, int i, int j){
    while(i < j){
        if(str[i] != ' '){
            return i;
        }
        i += 1;
    }
    return i;
}

int findLastChar(string & str, int i, int j){
    while((i < j) && (str[i] != ' ')){
        i += 1;
    }
    return i - 1;
}


class Solution {
public:
    string reverseWords(string s) {
        int n = s.size();
        
        int i = 0;
        while(i < n){
            int start = findFirstChar(s, i, n);
            int last = findLastChar(s, start, n);
            reverseString(s, start, last + 1);
            i = last + 1;
        }
        return s;
    }
};
