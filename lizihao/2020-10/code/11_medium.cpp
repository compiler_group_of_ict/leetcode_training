// 11. 盛最多水的容器
// https://leetcode-cn.com/problems/container-with-most-water/

class Solution {
public:
    int maxArea(vector<int>& height) {
        int i = 0;
        int j = height.size() - 1;
        int result = INT_MIN;
        while(i < j){
            if(height[i] < height[j]){
                result = std::max(result, height[i] * (j - i));
                i += 1;
            } else {
                result = std::max(result, height[j] * (j - i));
                j -= 1;
            }
        }
        return result;
    }
};
