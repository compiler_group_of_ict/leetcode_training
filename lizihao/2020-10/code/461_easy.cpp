// 461. 汉明距离
// https://leetcode-cn.com/problems/hamming-distance/

class Solution {
public:
    int hammingDistance(int x, int y) {
        int n = x ^ y;
        int count = 0;
        for(auto n = x ^ y; n != 0; n = n & (n - 1)){
            ++count; 
        }
        return count;
    }
};
