// 925. 长按键入
// https://leetcode-cn.com/problems/long-pressed-name/

class Solution {
public:
    bool isLongPressedName(string name, string typed) {
        int n1 = name.size();
        int n2 = typed.size();
        if(n1 > n2)
            return false;

        int i = 0, j = 0;
        while(i < n1 && j < n2){
            if(name[i] == typed[j]){
                i++;
                j++;
            }
            else if(j >= 1 && typed[j] == typed[j - 1]){
                j++;
            } else {
                return false;
            }
        }

        if(i != n1)
            return false;
        else{
            while(j < n2){
                if(typed[j] != name[n1 - 1])
                    return false;
                j++;
            }
            return true;
        }
    }
};
