// 145. 二叉树的后序遍历
// https://leetcode-cn.com/problems/binary-tree-postorder-traversal/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

void postorderTraversalNonEmpty(vector<int>& result, TreeNode* root){
    if(root->left)
        postorderTraversalNonEmpty(result, root->left);
    if(root->right)
        postorderTraversalNonEmpty(result, root->right);
    result.push_back(root->val);
}

class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> retval;
        if(root)
            postorderTraversalNonEmpty(retval, root);
        return retval;
    }
};
