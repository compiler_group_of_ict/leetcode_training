// 647. 回文子串
// https://leetcode-cn.com/problems/palindromic-substrings/

class Solution {
public:
    int countSubstrings(string s) {
        int n = s.size();
        if(n < 2)
            return n;

        int count = 0;        
        for(int i = 0; i < n; i++){
            for(int start = i, end = i; 
                start >= 0 && end < n && s[start] == s[end];
                start -= 1, end += 1)
            {
                count += 1;    
            }
        }

        for(int i = 1; i < n; i++){
            for(int start = i - 1, end = i; 
                start >= 0 && end < n && s[start] == s[end];
                start -= 1, end += 1)
            {
                count += 1;    
            }
        }

        return count;
    }
};
