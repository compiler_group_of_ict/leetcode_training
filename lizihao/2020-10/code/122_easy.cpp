// 122. 买卖股票的最佳时机 II
// https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        deque<int> ascent;
        ascent.push_back(INT_MAX);
        // ascent: from front to back, the price is higher and higher

        int result = 0;
        for(auto price : prices){
            if(price < ascent.back()){ 
                // Status: price < current_max_price, the price began to be descent
                if(ascent.size() >= 2){
                    // Status: there is more than one day to be ascent, it's time to sell
                    result += ascent.back() - ascent.front();
                }
                ascent.clear();
            }
            ascent.push_back(price);
        }
        result += ascent.back() - ascent.front();
        ascent.clear();
        return result;
    }
};
