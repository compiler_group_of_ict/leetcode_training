// 124. 二叉树中的最大路径和
// https://leetcode-cn.com/problems/binary-tree-maximum-path-sum/


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
    int result;
public:
    int maxPath(TreeNode * root){
        if(nullptr == root)
            return 0;
        
        int left = std::max(maxPath(root->left), 0);
        int right = std::max(maxPath(root->right), 0);
        
        result = std::max(result, root->val + left + right);

        return root->val + std::max(left, right);
    }

    int maxPathSum(TreeNode* root) {
        result = INT_MIN;
        maxPath(root);
        return result;
    }
};
