// 1365. 有多少小于当前数字的数字
// https://leetcode-cn.com/problems/how-many-numbers-are-smaller-than-the-current-number/

class Solution {
    enum {SIZE = 101};
    int count[SIZE];
public:
    vector<int> smallerNumbersThanCurrent(vector<int>& nums) {
        for(auto i = 0; i < SIZE; i++)
            count[i] = 0;

        for(const auto & x : nums){
            count[x] += 1;
        }

        int total = 0;
        for(auto i = 0; i < SIZE; i++){
            total += count[i];
            count[i] = total - count[i];
        }

        std::vector<int> result(nums.size());
        for(int i = 0; i < nums.size(); i++){
            result[i] = count[nums[i]];
        }
        return std::move(result);
    }
};
