// 590. N叉树的后序遍历
// https://leetcode-cn.com/problems/n-ary-tree-postorder-traversal/

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    void postorderIter(Node * root, vector<int> & result){
        for(auto child : root->children){
            if(!child)
                continue;
            postorderIter(child, result);
        }
        result.push_back(root->val);
    }

    vector<int> postorder(Node* root) {
        vector<int> result;
        if(root)
            postorderIter(root, result);
        return std::move(result);
    }
};
