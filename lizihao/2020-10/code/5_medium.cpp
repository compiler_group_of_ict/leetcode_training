// 5. 最长回文子串
// https://leetcode-cn.com/problems/longest-palindromic-substring/


class Solution {
public:
    string longestPalindrome(string s) {
       int n = s.size();
        if(n < 2)
            return s;
        
        int begin = 0;
        int length = 1;

        for(int i = 1; i < n; i++){
            int start = i - 1;
            int end = i + 1;
            while(start >= 0 && end < n && s[start] == s[end]){
                start -= 1;
                end += 1;
            }

            if(end - start - 1 > length){
                length = end - start - 1;
                begin = start + 1;
            }
            
            start = i - 1;
            end = i;
            while(start >= 0 && end < n && s[start] == s[end]){
                start -= 1;
                end += 1;
            }

            if(end - start - 1 > length){
                length = end - start - 1;
                begin = start + 1;
            }
        }
        
        return string(s.begin() + begin, s.begin() + begin + length);
    }
};
