// 226. 翻转二叉树
// https://leetcode-cn.com/problems/invert-binary-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
void utility(TreeNode* root) {
    swap(root->left, root->right);
    if(root->left)
        utility(root->left);
    
    if(root->right)
        utility(root->right);
}

class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        if(root)
            utility(root);
        return root;
    }
};
