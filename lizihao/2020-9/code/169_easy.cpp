// 169. 多数元素 
// https://leetcode-cn.com/problems/majority-element/

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int count = 0;
        int candidate;
        for(auto & num : nums){
            if(count == 0){
                candidate = num;
            }

            count += (candidate == num) ? 1 : -1;
        }
        return candidate;
    }
};
