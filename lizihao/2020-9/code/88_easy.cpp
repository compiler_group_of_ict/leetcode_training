// 88. 合并两个有序数组
// https://leetcode-cn.com/problems/merge-sorted-array/

class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        if(n < 1)
            return;
        for(int i = 0; i < m; i++){
            if(nums2[0] < nums1[i]){
                int val = nums1[i];
                nums1[i] = nums2[0];

                int t;
                for(t = 1; t < n; t++){
                    if(nums2[t] >= val)
                        break;
                    nums2[t - 1] = nums2[t];                    
                }
                nums2[t - 1] = val;
            }
        }

        for(int i = 0; i < n; i++)
            nums1[m + i] = nums2[i];
    }
};
