// 102. 二叉树的层次遍历 中等
// https://leetcode-cn.com/problems/binary-tree-level-order-traversal/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {

public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> retval;
        if(root){
            queue<TreeNode*> q;
            q.push(root);
            int level = 0;
            while(!q.empty()){
                retval.push_back(vector<int> ());
                // visit nodes that in same level, and push their children into queue
                int n = q.size();
                for(int i = 0; i < n; i++){
                    auto ptr = q.front();
                    q.pop();
                    retval[level].push_back(ptr->val);
                    if(ptr->left)
                        q.push(ptr->left);
                    if(ptr->right)
                        q.push(ptr->right);
                }
                level += 1;
            }
        }
        return retval;
    }
};
