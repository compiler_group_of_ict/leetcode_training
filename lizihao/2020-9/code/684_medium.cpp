// 684. 冗余连接
// https://leetcode-cn.com/problems/redundant-connection/

class UnionFind
{
    vector<int> parent;
    vector<int> rank;
public:
    UnionFind(int n)
    {
        parent.resize(n + 1, -1);
        rank.resize(n + 1, 0);
    }

    int findSet(int i)
    {
        if(parent[i] == -1)
            return i;
        else
        {
            int result = findSet(parent[i]);
            parent[i] = result;
            return result;
        }
    }
    
    // id_i != id_j    
    void unionSets(int id_i, int id_j)
    {
        if(rank[id_i] < rank[id_j])
        {
            rank[id_j] += 1;
            parent[id_i] = id_j;
        }
        else 
        {
            rank[id_i] += 1;
            parent[id_j] = id_i;
        }
    }
};

class Solution {
public:
    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        int n = edges.size();
        UnionFind disjoint_set(n);

        vector<int> result;

        for(int i = 0; i < n; i++){
            int id_i = disjoint_set.findSet(edges[i][0]);
            int id_j = disjoint_set.findSet(edges[i][1]);

            if(id_i == id_j){
                result.push_back(min(edges[i][0], edges[i][1]));
                result.push_back(max(edges[i][0], edges[i][1]));
                return result;
            } else {
                disjoint_set.unionSets(id_i, id_j);
            }
        }
        return result;
    }
};
