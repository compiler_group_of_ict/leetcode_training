// 538. 把二叉搜索树转换为累加树
// https://leetcode-cn.com/problems/convert-bst-to-greater-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    int sum;

    void convertBSTUtil(TreeNode* root){
        if(root->right){
            convertBSTUtil(root->right);
        }
        
        sum += root->val;
        root->val = sum;

        if(root->left){
            convertBSTUtil(root->left);
        }
    }
public:
    TreeNode* convertBST(TreeNode* root) {
        sum = 0;

        if(root)
            convertBSTUtil(root);
        return root;
    }
};


