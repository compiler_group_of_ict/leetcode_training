// 217. 存在重复元素
// https://leetcode-cn.com/problems/contains-duplicate/



inline int rand_int(int n){
    return (rand() % n);
}

class Solution {
public:
    Solution(){
        srand(time(NULL));
    }

    bool containsDuplicate(vector<int>& nums) {
        int n = nums.size();
        if(n < 2)
            return false;
        
        sort(nums.begin(), nums.end());
        int pred = nums[0];
        for(int i = 1; i < n; i++){
            if(nums[i] == pred)
                return true;
            else{
                pred = nums[i];
            }
        }

        return false;

    }
};
