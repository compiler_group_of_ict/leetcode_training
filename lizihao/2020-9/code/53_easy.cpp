// 53. 最大子序和
// https://leetcode-cn.com/problems/maximum-subarray/

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        int result = nums[0];        
        for(int i = 1; i < n; i ++){
            nums[i] = max(nums[i], nums[i - 1] + nums[i]);
            result = max(result, nums[i]);
        }
        return result;
    }
};
