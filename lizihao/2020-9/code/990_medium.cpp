// 990. 等式方程的可满足性
// https://leetcode-cn.com/problems/satisfiability-of-equality-equations/



class CharacterUnionFind{
private:
    enum {LENGTH = 256};
    char parent[LENGTH];
    int rank[LENGTH];

public:
    CharacterUnionFind(){
        for(char i = 'a'; i <= 'z'; i++){
            parent[i] = i;
            rank[i] = 0;
        }
    }

    char find_set(char ch){
        if(parent[ch] == ch)
            return ch;
        else{
            char result = find_set(parent[ch]);
            parent[ch] = result;
            return result;
        }
    }

    void union_sets(char ch1, char ch2){
        char idt1 = find_set(ch1);
        char idt2 = find_set(ch2);

        int rank1 = rank[idt1];
        int rank2 = rank[idt2];

        if(idt1 == idt2) // ch1 and ch2 are in same set
            return;

        if(rank1 < rank2){
            parent[idt1] = idt2;
        } else if(rank1 > rank2){
            parent[idt2] = idt1;
        } else { // rank1 == rank2
            parent[idt1] = idt2;
            rank[idt2] += 1;
        }
    }
};

class Solution {
public:
    bool equationsPossible(vector<string>& equations) {
        CharacterUnionFind obj;

        for(const string & equation : equations){
            const char * str = equation.data();

            if(str[1] == '='){
                obj.union_sets(str[0], str[3]);
            }
        }

        for(const string & equation : equations){
            const char * str = equation.data();

            if(str[1] == '!'){
                // 'a' is supposed to be not equal to 'b'
                if(obj.find_set(str[0]) == obj.find_set(str[3]))
                    return false;
            }
        } 

        return true;
    }
};
