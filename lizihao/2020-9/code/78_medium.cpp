// 78. 子集
// https://leetcode-cn.com/problems/subsets/


class Solution {
    void subsetsUtil(vector<int>& nums, int idx, vector<int>& buf, vector<vector<int>> & result){
        if(idx == nums.size()){
            result.push_back(buf);
            return;
        }
        
        subsetsUtil(nums, idx + 1, buf, result);

        buf.push_back(nums[idx]);
        subsetsUtil(nums, idx + 1, buf, result);
        buf.pop_back();
    }
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<int> buf;
        vector<vector<int>> result;

        subsetsUtil(nums, 0, buf, result);
        return result;
    }
};
