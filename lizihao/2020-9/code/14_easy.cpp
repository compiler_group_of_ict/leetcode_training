// 14. 最长公共前缀
// https://leetcode-cn.com/problems/longest-common-prefix/

class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        int n = strs.size();
        string result;
        if(n == 0)
            return result;
        int idx = 0;
        for(;;){
            char ch = strs[0][idx];
            if(ch == '\0')
                return result;
            for(int i = 1; i < n; i++){
                char tmp = strs[i][idx];
                if(tmp == '\0' || tmp != ch)
                    return result;
            }
            result.push_back(ch);
            idx += 1;
        }
        return result;
    }
};
