// 501. 二叉搜索树中的众数
// https://leetcode-cn.com/problems/find-mode-in-binary-search-tree/
 
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */


class Solution {
public:
    vector<int> findMode(TreeNode* root) {
        vector<int> retval;

        int maxCount = 0;

        int zhongshu = -1;
        int count = 0;

        TreeNode * cur = root;
        while(cur){
            if(cur->left == nullptr){
                // visit value
                if(count == 0){
                    count = 1;
                    zhongshu = cur->val;
                    maxCount = 1;
                    retval.push_back(zhongshu);
                } else {
                    if(cur->val == zhongshu){
                        count += 1;
                    } else {
                        count = 1;
                        zhongshu = cur->val;
                    }

                    if(count == maxCount){
                        retval.push_back(cur->val);
                    } else if(count > maxCount){
                        maxCount = count;
                        retval.clear();
                        retval.push_back(cur->val);
                    }
                }       
                cur = cur->right;
            } else {
                // Condition : cur->left != nullptr
                TreeNode * rightmost = cur->left;
                while(rightmost->right)
                    rightmost = rightmost->right;
                rightmost->right = cur;

                TreeNode * tmp = cur->left;
                cur->left =nullptr;
                cur = tmp;
            }
        }        
        return retval;
    }
};
