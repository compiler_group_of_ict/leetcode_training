// 1588. 所有奇数长度子数组的和
// https://leetcode-cn.com/problems/sum-of-all-odd-length-subarrays/


// O(n^2)
/*class Solution {
public:
    int sumOddLengthSubarrays(vector<int>& arr) {
        int n = arr.size();
        vector<int> cum_sum;
        cum_sum.push_back(0);

        for(int i = 0; i < n; i++)
            cum_sum.push_back(cum_sum.back() + arr[i]);

        int result = cum_sum.back();
        for(int len = 3; len <= n; len += 2){
            int max_i = n - len;
            for(int i = 0; i <= max_i; i++){
                // arr[i + len - 1] ~ arr[i]
                result += cum_sum[i + len] - cum_sum[i];
            }
        }
        return result;
    }
}; */


// O(n)

inline int even_num(int i){
    // i >= 0
    // 返回 0 ~ i 中偶数的个数
    return 1 + i/2;
}

inline int odd_num(int i){
    // i >= 0
    // 返回 0 ~ i 中奇数的个数
    return (1 + i) / 2;
}

class Solution {
public:
    int sumOddLengthSubarrays(vector<int>& arr) {
        int n = arr.size();
        vector<int> times(n, 0);

        
        for(int i = 0; i < n; i++){ 
            times[i] += even_num(i) * even_num(n - i - 1); // 前面选偶数个，后面选偶数个
            times[i] += odd_num(i) * odd_num(n - i - 1);   // 前面选奇数数个，后面选奇数个
        }

        int result =  0;
        for(int i = 0; i < n; i++){
            result += arr[i] * times[i];
        }
        return result;
    }
};
