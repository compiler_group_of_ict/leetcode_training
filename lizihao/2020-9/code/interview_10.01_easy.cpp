// 面试题 10.01. 合并排序的数组
// https://leetcode-cn.com/problems/sorted-merge-lcci/

class Solution {
public:
    void merge(vector<int>& A, int m, vector<int>& B, int n) {
        if(n < 1)
            return;

        int i;
        for(i = 0; i < m; i++)
        {
            if(B[0] < A[i]){
                std::swap(B[0], A[i]);
                auto key = B[0];
                auto pos = copy_if(B.begin() + 1, B.end(), B.begin(), [key](int x){
                    return x < key;
                });
                *pos = key;
            }
        }

        copy(B.begin(), B.end(), A.begin() + m);
    }
};
