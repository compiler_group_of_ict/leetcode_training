// 1038. 把二叉搜索树转换为累加树
// https://leetcode-cn.com/problems/binary-search-tree-to-greater-sum-tree/


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
    int sum;
    
    void iter(TreeNode * root){
        if(auto right = root->right)
            iter(right);
        sum += root->val;
        root->val = sum;
        if(auto left = root->left)
            iter(left);
    }
public:
    TreeNode* bstToGst(TreeNode* root) {
        if(root)
            iter(root);
        return root;
    }
};
