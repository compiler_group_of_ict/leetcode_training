// 剑指 Offer 42. 连续子数组的最大和
// https://leetcode-cn.com/problems/lian-xu-zi-shu-zu-de-zui-da-he-lcof/

// cumsum[j] - cum[i - 1]

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        int result = nums[0];
        for(int i = 1; i < n; i++){
            nums[i] = max(nums[i], nums[i] + nums[i - 1]);
            result = max(nums[i], result);
        }
        return result;
    }
};
