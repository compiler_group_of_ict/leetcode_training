// 141. 环形链表
// https://leetcode-cn.com/problems/linked-list-cycle/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        if(nullptr == head)
            return false;
        
        ListNode * fast = head->next;
        ListNode * slow = head;

        while(slow != fast){
            if(fast == nullptr || fast->next == nullptr)
                return false;

            fast = fast->next->next;
            slow = slow->next;
        }
        return true;
    }
};
