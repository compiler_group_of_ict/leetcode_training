// 69. x 的平方根
// https://leetcode-cn.com/problems/sqrtx/

class Solution {
public:
    int mySqrt(int x) {
        int i = 1;
        while(i  <= x/i){
            i += 1;
        }
        return i - 1;
    }
};
