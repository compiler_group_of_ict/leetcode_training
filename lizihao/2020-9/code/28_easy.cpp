// 28. 实现 strStr()
// https://leetcode-cn.com/problems/implement-strstr/

class Solution {
public:
    int strStr(string haystack, string needle) {
        int n = haystack.size();
        int m = needle.size();
        if(m == 0)
            return 0;

        char start = needle[0];

        for(int i = 0; i <= n - m; i++){
            if(haystack[i] == start){
                int j;
                for(j = 1; j < m; j++){
                    if( haystack[i + j] != needle[j])
                        break;
                }
                if(j == m)
                    return i;
            }
        }
        return -1;
    }
};
