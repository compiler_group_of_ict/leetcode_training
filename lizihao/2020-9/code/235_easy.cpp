// 235. 二叉搜索树的最近公共祖先
// https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

TreeNode* lowestCommonAncestorIter(TreeNode * root, int v1, int v2){
    int val = root->val;
    if(v1 <= val && v2 >= val)
        return root;
    
    return (v2 < val) ? lowestCommonAncestorIter(root->left, v1, v2)
                      : lowestCommonAncestorIter(root->right, v1, v2);
}

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        int v1 = p->val;
        int v2 = q->val;
        return v1 < v2 ? lowestCommonAncestorIter(root, v1, v2) : lowestCommonAncestorIter(root, v2, v1);
    }
};
