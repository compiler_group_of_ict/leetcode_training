// 172. 阶乘后的零
// https://leetcode-cn.com/problems/factorial-trailing-zeroes/

class Solution {
public:
    int trailingZeroes(int n) {
        int five = 0;
        for(int i = 1; 5 * i <= n; i++){
            five += 1;
            int tmp = i;
            while(tmp % 5 == 0){
                tmp /= 5;
                five += 1;
            }
        }
        return five;
    }
};

