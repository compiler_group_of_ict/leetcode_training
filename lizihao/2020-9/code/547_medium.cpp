// 547. 朋友圈
// https://leetcode-cn.com/problems/friend-circles/


class UnionFind{
private:
    enum {MAX_LENGTH = 200};
    int parent[MAX_LENGTH];
    int rank[MAX_LENGTH];
    int group_count;

public:
    UnionFind(int n) {
        group_count = n;
        for(int i = 0; i < n; i++){
            rank[i] = 0;
            parent[i] = i;
        }
    }

    int find(int i){
        if(parent[i] == i)
            return i;
        else{
            int result = find(parent[i]);
            parent[i] = result;
            return result;
        }
    }

    void merge(int i, int j){
        int id1 = find(i);
        int id2 = find(j);

        if(id1 == id2)
            return; // these two are in same group

        int rank1 = rank[id1];
        int rank2 = rank[id2];
        if(rank1 < rank2){
            parent[id1] = id2;
        } else if(rank2 < rank1){
            parent[id2] = id1;
        } else{
            parent[id2] = id1;
            rank[id1] += 1;
        }

        group_count -= 1;
    }

    inline int count(){ return group_count; }
};

class Solution {
public:
    int findCircleNum(vector<vector<int>>& M) {
        int n = M.size();

        UnionFind disjoint(n);

        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(M[i][j])
                    disjoint.merge(i, j);
            }
        }

        return disjoint.count();
    }
};
