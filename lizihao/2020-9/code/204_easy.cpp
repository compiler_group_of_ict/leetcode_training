// 204. 计数质数
// https://leetcode-cn.com/problems/count-primes/

class Solution {
public:
    int countPrimes(int n) {
        if(n < 3)
            return 0;
        else{
            vector<bool> is_prime(n, true);
            int result = 1;
            int root = std::sqrt(n + 1);
            for(int i = 3; i <= root; i += 2){
                if(is_prime[i]){
                    result += 1;
                    for(int t = i + i; t < n; t += i)
                        is_prime[t] = false;
                }
            }

            root = root % 2 == 0 ? root + 1 : root + 2;
            for(int i = root; i < n; i+=2)
                result += is_prime[i] ? 1 : 0;

            return result;
        }
    }
};
