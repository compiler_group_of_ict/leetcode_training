// 160. 相交链表
// https://leetcode-cn.com/problems/intersection-of-two-linked-lists/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

int length(ListNode * head){
    int result = 0;
    while(head){
        result += 1;
        head = head->next;
    }
    return result;
}

ListNode * advance(ListNode * head, int n){
    while(n-- > 0){
        head = head->next;
    }
    return head;
}

class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int t = length(headA) - length(headB);
        if(t < 0){
            headB = advance(headB, -t);
        } else {
            headA = advance(headA, t);
        }

        while(headA){
            if(headA == headB){
                /*while(headA->next && (headA->next->val == headB->next->val)){
                    headA = headA->next;
                    headB = headB->next;
                    return headA;
                }*/
                return headA;
            }
            headA = headA->next;
            headB = headB->next;
        }

        return nullptr;
    }
};
