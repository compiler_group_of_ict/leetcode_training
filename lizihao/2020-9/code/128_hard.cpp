// 128. 最长连续序列
// https://leetcode-cn.com/problems/longest-consecutive-sequence/

int find(unordered_map<int, pair<int, int>> & data, int i){
    // Pre-condtion: data[i] must exist
    int parent = data[i].first;

    if(parent == i)
        return i;
    else{
        int result = find(data, parent);
        data[i].first = result;
        return result;
    }
}

void merge(unordered_map<int, pair<int, int>> & data, int i, int j){
    // Pre-condtion: data[i] and data[j] must exist

    int id1 = find(data, i);
    int id2 = find(data, j);

    if(id1 == id2)
        return; // these two are in same group
    
    int size1 = data[id1].second;
    int size2 = data[id2].second;

    if(size1 < size2){
        data[id1].first = id2; // parent[id1] -> id2
        data[id2].second += size1; // size[id2] += size[id1]
    } else { // size1 >= size2
        data[id2].first = id1; // parent[id2] -> id1
        data[id1].second += size2; // size[id1] += size[id2]
    }
}

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_map<int, pair<int, int>> data; // key -> (parent, group_size)

        // initialize
        for(auto num : nums){
            data.insert(make_pair(num, make_pair(num, 1)));
        }

        // pass
        for(auto num : nums){
            if(data.find(num - 1) != data.end()){
                merge(data, num - 1, num);
            }

            if(data.find(num + 1) != data.end()){
                merge(data, num + 1, num);
            }
        }

        // find max group
        int length = 0;
        for(auto pr : data){
            if(pr.second.second > length)
                length = pr.second.second;
        }
        return length;
    }
};
