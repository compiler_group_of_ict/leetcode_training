// 118. 杨辉三角
// https://leetcode-cn.com/problems/pascals-triangle/

class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        int n = numRows;

        vector<vector<int>> retval(n);
        for(int i = 0; i < n; i++)
            retval[i].resize(i + 1, 1);

        for(int i = 2; i < n; i++){
            for(int j = 1; j < i; j++){
                retval[i][j] = retval[i - 1][j - 1] + retval[i - 1][j];
            }
        }
        return retval;
    }
};
