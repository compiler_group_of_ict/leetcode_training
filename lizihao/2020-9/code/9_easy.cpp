// 9. 回文数
// https://leetcode-cn.com/problems/palindrome-number/

class Solution {
public:
    bool isPalindrome(int x) {
        if(x < 0 || (x % 10 == 0 && x != 0))
            return false;
        string str = to_string(x);
        int n = str.size();
        int max_i = n/2;
        for(int i = 0; i <= max_i; i++){
            if(str[i] != str[n - i - 1])
                return false;
        }
        return true;
    }
};

