// 292. Nim 游戏
// https://leetcode-cn.com/problems/nim-game/

class Solution {
public:
    bool canWinNim(int n) {
        return (n <= 0) ? false : ((n & 3) != 0);
    }
};
