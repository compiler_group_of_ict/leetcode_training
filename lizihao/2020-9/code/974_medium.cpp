// 974. 和可被 K 整除的子数组
// https://leetcode-cn.com/problems/subarray-sums-divisible-by-k/

// Def. cumsum[i] = SUM(A[0] ~ A[i - 1])
//     for 0 <= i <= j < n, 
//     SUM(A[i] ~ A[j]) = cumsum[j + 1] - cum[i]
//   find SUM(A[i] ~ A[j]) % K == 0 <=> find (cumsum[j + 1] % K) == (cumsum[i] % K)

class Solution {
public:
    int subarraysDivByK(vector<int>& A, int K) {
        int n = A.size();
        int sum = 0;
        int count = 0;
        
        unordered_map<int, int> tbl;
        tbl[0] = 1;

        for(int i = 0; i < n; i++){
            sum += A[i];
            int m = ((sum % K) + K) % K;
            if(tbl.find(m) != tbl.end()){
                count += tbl[m];
                tbl[m] += 1;
            } else {
                tbl[m] = 1;
            }
        }
        return count;
    }
};

