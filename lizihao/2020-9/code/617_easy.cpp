// 617. 合并二叉树
// https://leetcode-cn.com/problems/merge-two-binary-trees/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
        if(!t1 && !t2){
            return nullptr;
        } else if(t1 && t2){
            auto left1 = t1->left;
            auto left2 = t2->left;
            auto right1 = t1->right;
            auto right2 = t2->right;

            t1->val += t2->val;
            delete t2;
            t1->left = mergeTrees(left1, left2);
            t1->right = mergeTrees(right1, right2);
            return t1;
        } else {
            return t1  ? t1 : t2;
        }    
    }
};
