// 7. 整数反转
// https://leetcode-cn.com/problems/reverse-integer/

int reverseNonnegative(unsigned long x) {
    unsigned long result = 0;
    while(x > 0){
        result = result * 10UL + (x % 10);
        x /= 10;
    }
    if(result > INT_MAX)
        return 0;
    return result;
}

class Solution {
public:
    int reverse(int x) {
        long tmp = x;
        if(tmp < 0){
            tmp = -tmp;
            if(tmp > INT_MAX)
                return 0;
            else
                return -reverseNonnegative(tmp);
        }
        return reverseNonnegative(x);
    }
};
