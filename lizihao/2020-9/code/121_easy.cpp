// 121. 买卖股票的最佳时机
// https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        if(n <= 1)
            return 0;
        
        int profit = 0;
        int min_price = INT_MAX;
        for(int i = 0; i < n; i++){
            if(prices[i] < min_price)
                min_price = prices[i];
            else
                profit = max(profit, prices[i] - min_price);
        }
        return profit;
    }
};

