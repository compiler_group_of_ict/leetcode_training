// 面试题 17.07. 婴儿名字
// https://leetcode-cn.com/problems/baby-names-lcci/

class UnionFind{
public:
    vector<int> parent;
    UnionFind(int n){
        parent.resize(n, -1);
    }

    int utilityFindSet(int i){
        if(parent[i] == -1)
            return i;
        else{
            int result = utilityFindSet(parent[i]);
            parent[i] = result;
            return result;
        }
    }

    void utilityUnionSet(vector<string> & names, vector<int> &frequence, int i, int j){
        int id_i = utilityFindSet(i);
        int id_j = utilityFindSet(j);

        if(id_i == id_j)
            return;

        if(names[id_i] < names[id_j]){
            parent[id_j] = id_i;
            frequence[id_i] += frequence[id_j];
        } else {
            parent[id_i] = id_j;
            frequence[id_j] += frequence[id_i];
        }
    }
};

class Solution {
public:
    vector<string> trulyMostPopular(vector<string>& names, vector<string>& synonyms) {
        int n = names.size();
        
        // for Union Find
        vector<int> frequence(n, 0);
        vector<string> names_saved;

        unordered_map<string, int> name2id; // Map(name -> int_key)
        for(int i = 0; i < n; i++){
            int paren_start = names[i].find_last_of('(');
            names_saved.push_back(names[i].substr(0, paren_start));
            name2id[names_saved[i]] = i;

            int integer_len = names[i].size() - paren_start - 2;
            int freq_i = stoi(names[i].substr(paren_start + 1, integer_len));
            frequence[i] = freq_i;
        }

        UnionFind disjoin_sets(n);
        for(auto & syn : synonyms){
            int idx_comma = syn.find_first_of(',');
            int id_name1 = name2id[syn.substr(1, idx_comma - 1)];
            int id_name2 = name2id[syn.substr(idx_comma + 1, syn.size() - idx_comma - 2)];
            
            disjoin_sets.utilityUnionSet(names_saved, frequence,id_name1, id_name2);
        }

        vector<string> result;
        for(int i = 0; i < n; i++){
            if(disjoin_sets.parent[i] == -1){
                names_saved[i].push_back('(');
                names_saved[i] += to_string(frequence[i]);
                names_saved[i].push_back(')');
                result.push_back(move(names_saved[i]));
            }
        }
        return result;
    }
};
