// 20. 有效的括号
// https://leetcode-cn.com/problems/valid-parentheses/

class Solution {
public:
    bool isValid(string s) {
        vector<char> buf;
        for(auto ch : s){
            switch(ch){
            case '(':
            case '{':
            case '[':
                buf.push_back(ch);
                break;
            case ')':
                if(buf.empty() || buf.back() != '(')
                    return false;
                else 
                    buf.pop_back();
                break;
            case ']':
                if(buf.empty() || buf.back() != '[')
                    return false;
                else 
                    buf.pop_back();
                break;
            case '}':
                if(buf.empty() || buf.back() != '{')
                    return false;
                else 
                    buf.pop_back();
                break;
            }
        }

        return buf.empty();
    }
};
