// 765. 情侣牵手
// https://leetcode-cn.com/problems/couples-holding-hands/

class Solution {
public:
    int minSwapsCouples(vector<int>& row) {
        int n = row.size();
        vector<int> pos(n);
        vector<int> partner(n);

        for(int i = 0; i < n; i++){
            pos[row[i]] = i;
            partner[row[i]] = (row[i] % 2 == 1) ? row[i] - 1 : row[i] + 1;
        }

        int result = 0;
        for(int i = 0; i < n; i += 2){
            if(partner[row[i]] == row[i + 1])
                continue;
            else{
                // exchange partner[row[i]] with row[i + 1]
                int exchange_value = partner[row[i]];
                int old_pos = pos[exchange_value];
                int new_pos = i + 1;

                swap(row[old_pos], row[new_pos]);
                pos[exchange_value] = new_pos;
                pos[row[old_pos]] = old_pos;
                
                result += 1;
            }
        }

        return result;
    }
};
