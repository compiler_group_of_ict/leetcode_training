// 560. 和为K的子数组
// https://leetcode-cn.com/problems/subarray-sum-equals-k/

// Def. Cumsum[i] = SUM(nums[0], nums[1], nums[2], ..., nums[i])
//      =>  SUM[nums[i] ~ nums[j]] = Cumsum[j] - Cumsum[i - 1]
//  for 0 <= i <= j < n, find the times that "Cumsum[j] == k + Cumsum[i - 1]" is true

class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> tbl;
        tbl[0] = 1;
        
        int sum = 0;
        int n = nums.size();
        int result = 0;
        for(int i = 0; i < n; i++){
            sum += nums[i];

            if(tbl.find(sum - k) != tbl.end()){
                result += tbl[sum - k];
            }

            if(tbl.find(sum) != tbl.end())
                tbl[sum] += 1;
            else
                tbl[sum] = 1;
        }

        return result;

    }
};
