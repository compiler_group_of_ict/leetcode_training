// 21. 合并两个有序链表
// https://leetcode-cn.com/problems/merge-two-sorted-lists/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

ListNode * reverse(ListNode * root){
    ListNode * result = nullptr;
    while(root){
        ListNode * next = root->next;
        root->next = result;
        result = root;
        root = next;
    }    
    return result;
}

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode * result = nullptr;
        
        ListNode* buf[2];
        buf[0] = l1;
        buf[1] = l2;

        while(buf[0] && buf[1]){
            int id = (buf[0]->val < buf[1]->val) ? 0 : 1;
            ListNode * next = buf[id]->next;
            buf[id]->next = result;
            result = buf[id];
            buf[id] = next;
        }

        if(buf[0] || buf[1]){
            int id = buf[0] != nullptr ? 0 : 1;
            while(buf[id]){
                ListNode * next = buf[id]->next;
                buf[id]->next = result;
                result = buf[id];
                buf[id] = next;
            }
        }

        return reverse(result);
    }
};
