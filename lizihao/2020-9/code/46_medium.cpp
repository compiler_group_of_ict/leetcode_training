// 46. 全排列
// https://leetcode-cn.com/problems/permutations/

class Solution {
    void permuteUtil(vector<int>& nums, int i, int n, vector<int>& buf, vector<bool>& visited, vector<vector<int>> & result){
        if(i == n){
            result.push_back(buf);
            return;
        }

        for(int t = 0; t < n; t++){
            if(!visited[t]){
                visited[t] = true;
                buf[i] = nums[t];
                permuteUtil(nums, i + 1, n, buf, visited, result);
                visited[t] = false;
            }
        }
    }
public:
    vector<vector<int>> permute(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> result;

        vector<int> buf(n);
        vector<bool> visited(n, false);

        permuteUtil(nums, 0, n, buf, visited, result);
        return result;
    }
};
