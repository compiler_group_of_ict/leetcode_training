// 26. 删除排序数组中的重复项
// https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size();
        if(n <= 1)
            return n;
        
        int t = 1; // nums[0 : t - 1], without duplication
        
            int pred = nums[0];
            for(int i = 1; i < n; i++){
                if(nums[i] != pred){
                    nums[t] = nums[i];
                    pred = nums[i];
                    t += 1;
                }
            }
        

        int len = n - t;
        for(int i = 0; i < len; i++)
            nums.pop_back();
        return t;

    }
};
