// 70. 爬楼梯
// https://leetcode-cn.com/problems/climbing-stairs/

class Solution {
public:
    int climbStairs(int n) {
        if(n <= 3)
            return n;
        
        int a0 = 2, a1 = 3;
        for(int i = 4; i <= n; i++){
            a0 += a1;
            swap(a0, a1);
        }
        return a1;
    }

};
