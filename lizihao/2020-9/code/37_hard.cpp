// 37. 解数独
// https://leetcode-cn.com/problems/sudoku-solver/

class Solution {
private:
    bool row_used[9][9];
    bool col_used[9][9];
    bool block_used[3][3][9];
    vector<pair<int, int>> space;
    
    inline bool num_is_availiable(int i, int j, int num){
        return !(row_used[i][num] || col_used[j][num] || block_used[i/3][j/3][num]);
    }

    inline void set_num_used(int i, int j, int num){
        row_used[i][num] = true;
        col_used[j][num] = true;
        block_used[i/3][j/3][num] = true;
    }

    inline void unset_num_used(int i, int j, int num){
        row_used[i][num] = false;
        col_used[j][num] = false;
        block_used[i/3][j/3][num] = false;
    }
    
    inline void initialize(vector<vector<char>>& board){
        for(int i = 0; i < 9; i++)
            for(int j = 0; j < 9; j++)
                for(int num = 0; num < 9; num++)
                    unset_num_used(i, j, num);

        for(int i = 0; i < 9; i++){
            for(int j = 0; j < 9; j++){
                if(board[i][j] != '.')
                    set_num_used(i, j, board[i][j] - '1');
                else
                    space.push_back(make_pair(i, j));
            }
        }
    }
public:
    void solveSudoku(vector<vector<char>>& board) {
        initialize(board);

        solve(board, 0);
    }

    bool solve(vector<vector<char>>& board, int idx){
        if(idx >= space.size())
            return true;

        int i = space[idx].first;
        int j = space[idx].second;

        for(int num = 0; num < 9; num ++){
            if(num_is_availiable(i, j, num)){
                board[i][j] = num + '1';
                set_num_used(i, j, num);

                if(solve(board, idx + 1) == true)
                    return true;
                else{
                   unset_num_used(i, j, num);
                }
            }
        }
        return false;
    }
};
