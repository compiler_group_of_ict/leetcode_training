// 136. 只出现一次的数字
// https://leetcode-cn.com/problems/single-number/

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int n = nums.size();
        int result = nums[0];
        for(int i = 1; i < n; i++)
            result ^= nums[i];
        return result;
    }
};
