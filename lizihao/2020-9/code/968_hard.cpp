// 968. 监控二叉树
// https://leetcode-cn.com/problems/binary-tree-cameras/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

enum status{
    kHaveCamera,
    kNoCameraButCoverd,
    kNoCameraNotCoverd
};

class Solution {
    int count = 0;
    status iter(TreeNode * root){
        if(root == nullptr)
            return kNoCameraButCoverd;
        
        auto left = iter(root->left);
        auto right = iter(root->right);
        if(left == kNoCameraNotCoverd || right == kNoCameraNotCoverd){
            count += 1;
            return kHaveCamera;
        }
        return (left == kHaveCamera || right == kHaveCamera) ? kNoCameraButCoverd:  
                                                               kNoCameraNotCoverd;

    }
public:
    int minCameraCover(TreeNode* root) {
        count = 0;
        if(iter(root) == kNoCameraNotCoverd)
            count += 1;
        return count;
    }
};
