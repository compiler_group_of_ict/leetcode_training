import os
import sys

def rename_all(root_path):
    file_names = os.listdir(root_path)
    for name in file_names:
        path = os.path.join(root_path, name)
        if (not os.path.isdir(path)) and (path[-3:] != 'cpp'):
            os.rename(path, path + '.cpp')



if __name__ == "__main__":
    if len(sys.argv) < 2:
        rename_all(".")
    else:
        rename_all(sys.argv[1])
