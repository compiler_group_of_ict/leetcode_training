class TreeDistanceSum{
    const vector<vector<int>> &adjList;
    const size_t N;
    vector<int> nodeNums;
    vector<int> dp;

    void recursivelyInit(int root, int parent){
        dp[root] = 0;
        nodeNums[root] = 1;
        for(const auto & adj : adjList[root]){
            if(parent == adj) continue;
            
            recursivelyInit(adj, root);

            const int nodeNumsAdj = nodeNums[adj];
            nodeNums[root] += nodeNumsAdj;
            dp[root] += dp[adj] + nodeNumsAdj;
        }
    }

    void backtrack(int root, int parent, vector<int> & result, int count){
        result[root] = dp[root];
        if(count == N)
            return;
        for(const auto & adj : adjList[root]){
            if(parent == adj) continue;
            // save status
            const int dpRoot = dp[root];
            const int dpAdj = dp[adj];
            const int nodeNumsRoot = nodeNums[root];
            const int nodeNumsAdj = nodeNums[adj];

            // change the root of the tree
            dp[root] -= dpAdj + nodeNumsAdj;
            nodeNums[root] -= nodeNumsAdj;
            dp[adj] += dp[root] + nodeNums[root];
            nodeNums[adj] += nodeNums[root];

            // backtracking
            backtrack(adj, root, result, count + 1);

            // reload status
            dp[root] = dpRoot;
            dp[adj] = dpAdj;
            nodeNums[root] = nodeNumsRoot;
            nodeNums[adj] = nodeNumsAdj;
        }
    }
public:
    TreeDistanceSum( const vector<vector<int>> &adj_list) : adjList{adj_list}, N{adj_list.size()}{
        dp.resize(N, 0);
        nodeNums.resize(N, 0);
        recursivelyInit(0, -1);
    }

    vector<int> compute(){
        vector<int> result(N, 0);
        backtrack(0, -1, result, 1);
        return std::move(result);
    }
};

class Solution {
public:
    vector<int> sumOfDistancesInTree(int N, vector<vector<int>>& edges) {
        vector<vector<int>> adjList(N);
        for(const auto &pr : edges){
            const auto &i = pr[0];
            const auto &j = pr[1];
            adjList[i].emplace_back(j);
            adjList[j].emplace_back(i);
        }

        TreeDistanceSum solver(adjList);
        return solver.compute();
    }
};
