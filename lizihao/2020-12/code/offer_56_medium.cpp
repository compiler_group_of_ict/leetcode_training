class Solution {
public:
    vector<int> singleNumbers(vector<int>& nums) {
        int all_xor = 0;
        for(auto num : nums){
            all_xor ^= num;
        }

        int lsb = (-all_xor) & (all_xor);
        int a = 0;
        for(auto num : nums){
            if(num & lsb){
                a ^= num;
            }
        }

        return vector<int>{a, a ^ all_xor};
    }
};

