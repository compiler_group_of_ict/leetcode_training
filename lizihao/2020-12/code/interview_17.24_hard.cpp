// 最大子数组的二维拓展

class Solution {
public:
    int colPreSum[201][201];

    vector<int> getMaxMatrix(vector<vector<int>>& matrix) {
        int matrixRow = matrix.size();
        int matrixCol = matrix.front().size();

        memset(colPreSum, matrixRow * 201 * sizeof(int), 0);
        for(int row = 0; row < matrixRow; ++row){
            const int rowPlus1 = row + 1;      // common sub-expr
            for(int col = 0; col < matrixCol; ++col){
                const int colPlus1 = col + 1;  // common sub-expr
                colPreSum[colPlus1][rowPlus1] = colPreSum[colPlus1][row] + matrix[row][col];
            }
        }

        int curMax = INT_MIN;
        int rowBegin = 0, rowEnd = 0, colBegin = 0, colEnd = 0;
        for(int row1 = 0; row1 < matrixRow; ++row1){
            for(int row2 = row1; row2 < matrixRow; ++row2){
                const int row2Plus1 = row2 + 1; // common sub-expr

                int curResult, curColBegin;
                // Handle Case when column == 0
                curResult = colPreSum[1][row2Plus1] - colPreSum[1][row1];
                curColBegin = 0;
                if(curResult > curMax){
                    curMax = curResult;
                    rowBegin = row1;
                    colBegin = 0;
                    rowEnd = row2;
                    colEnd = 0;
                }

                // Handle Cases when column > 0
                for(int col = 1; col < matrixCol; ++col){
                    const int colPlus1 = col + 1; // common sub-expr

                    bool canConcate = curResult > 0;  // is former result positive ?
                    curResult = colPreSum[colPlus1][row2Plus1] - colPreSum[colPlus1][row1] + (canConcate ? curResult : 0);
                    curColBegin = canConcate ? curColBegin : col;

                    // save result
                    if(curResult > curMax){
                        curMax = curResult;
                        rowBegin = row1;
                        colBegin = curColBegin;
                        rowEnd = row2;
                        colEnd = col;
                    }
                }
            }
        }

        return vector<int>{rowBegin, colBegin, rowEnd, colEnd};
    }
};
