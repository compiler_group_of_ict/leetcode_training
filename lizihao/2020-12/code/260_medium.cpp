class Solution {
public:
    inline int get_lsb(int bits){
        int result = 1;
        while(0 == (result & bits)){
            result <<= 1;
        }
        return result;
    }
    vector<int> singleNumber(vector<int>& nums) {
        int all_xor = 0;
        for(const auto &num : nums){
            all_xor ^= num;
        }


        int lsb = get_lsb(all_xor);
        int a = 0;
        for(const auto &num : nums){
            if(num & lsb)
                a ^= num;
        }

        return vector<int>{a, a ^ all_xor};
    }
};
