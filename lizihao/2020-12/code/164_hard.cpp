class Solution {
public:
    int **swapBuffer;
    Solution(){
        swapBuffer = new int*[2];
    }
    ~Solution(){
        delete []swapBuffer;
    }

    int maxBit(int x){
        int mask = 1 << 30;
        while(0 == (mask & x)){
            mask >>= 1;
        }
        return mask;
    }

    int maximumGap(vector<int>& nums) {
        int n = nums.size();
        if (n < 2) {
            return 0;
        }
        vector<int> tmp(nums);
        swapBuffer[0] = nums.data();
        swapBuffer[1] = tmp.data();
        int idx = 0;

        
        int bit = maxBit(*max_element(nums.begin(), nums.end()));
        int mask = 1;
        

        while(mask <= bit){
            vector<int> count(2, 0);
            int * src = swapBuffer[idx];
            for (int i = 0; i < n; ++i) {
                int key = (mask & src[i]) ? 1 : 0;
                ++count[key];
            }
            count[1] += count[0];

            idx ^= 1; 
            int *dst = swapBuffer[idx];
            for (int i = n - 1; i >= 0; --i) {
                int key = (mask & src[i]) ? 1 : 0;
                dst[--count[key]] = src[i];
            }
            
            mask <<= 1;
        }

        int result = 0;
        int * src = swapBuffer[idx];
        for(int i = 1; i < n; ++i){
            result = max(result, src[i] - src[i - 1]);
        }

        return result;
    }
};
