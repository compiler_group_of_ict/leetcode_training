    int divideAndConquer(int** buffer, int i, int j, int writeIdx){
        // cout << i << ',' << j << '\n';
        if(j - i <= 1)
            return 0;
        
        const auto nextIdx = writeIdx ^ 1;
        const auto mid = (i + j) >> 1; 

        int result = divideAndConquer(buffer, i, mid, nextIdx);
        result += divideAndConquer(buffer, mid, j, nextIdx);

        int * src = buffer[nextIdx];
        int * dst = buffer[writeIdx];

        int t = mid - 1;
        int r = j - 1;
        // count
        while(t >= i){
            while(r >= mid && (src[t] <= 2L * (long)src[r])){ --r; }
            // r < mid || r >= mid && src[t] > 2 * src[r]
            result += r < mid ? 0 : r - mid + 1;
            t--;
        }

        // merge
        int left = i, right = mid;
        int pos = i;
        while(left < mid || right < j){
            if(left == mid){
                dst[pos++] = src[right++];
            } else if(right == j || src[left] < src[right]){
                dst[pos++] = src[left++];
            } else {
                dst[pos++] = src[right++];
            }
        }
        return result;
    }

class Solution {
public:
    int ** swapBuffer;

    Solution(){
        swapBuffer = new int*[2];
    }

    ~Solution(){
        delete []swapBuffer;
    }

    int reversePairs(vector<int>& nums) {
        vector<int> buffer(nums);
        swapBuffer[0] = nums.data();
        swapBuffer[1] = buffer.data();
        return divideAndConquer(swapBuffer, 0, nums.size(), 0);
    }
};
