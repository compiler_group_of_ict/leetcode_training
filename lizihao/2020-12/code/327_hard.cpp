class Solution {
public:
    int divideAndConquer(vector<vector<long>>& prefixsum, int i, int j, int lower, int upper, int idx){
        if(j - i <= 1)
            return 0;

        int mid = (i + j) >> 1;
        int nextIdx = idx ^ 1;
        int result = divideAndConquer(prefixsum, i, mid, lower, upper, nextIdx);
        result += divideAndConquer(prefixsum, mid, j, lower, upper, nextIdx);

        auto& src = prefixsum[nextIdx];
        auto& dst = prefixsum[idx];
        
        // count ranges
        int t = i, start = mid, end = mid;
        while(t < mid){
            while(start < j && src[start] - src[t] < lower) ++start; 
            while(end < j && src[end] - src[t] <= upper) ++end;
            result += end - start;
            ++t;
        }

        // merge
        int left = i, right = mid;
        int target = i;
        while(left < mid || right < j){
            if(left == mid)
                dst[target++] = src[right++];
            else if(right == j)
                dst[target++] = src[left++];
            else if(src[left] < src[right])
                dst[target++] = src[left++];
            else
                dst[target++] = src[right++];
        }
        return result;
    }

    int countRangeSum(vector<int>& nums, int lower, int upper) {
        int n = nums.size();

        vector<vector<long>> prefixsum(2, vector<long>(n + 1, 0));
        for(int i = 0; i < n; ++i){
            prefixsum[0][i + 1] = prefixsum[0][i] + nums[i];
            prefixsum[1][i + 1] = prefixsum[0][i + 1];
        }

        return divideAndConquer(prefixsum, 0, n + 1, lower, upper, 0);
    }
};
