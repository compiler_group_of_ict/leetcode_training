struct Node{
    int start;
    int end;
    int profit;

    Node(int startTime, int endTime, int value): start(startTime), end(endTime), profit(value) {}
    Node(const Node &) = default;
};

class Solution {
public:
    int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
        // Add a dummy job
        startTime.push_back(0);
        endTime.push_back(0);
        profit.push_back(0);

        int n = startTime.size();
        vector<int> id(n);
        iota(id.begin(), id.end(), 0);
        sort(id.begin(), id.end(),[&endTime](auto a, auto b){
            return endTime[a] < endTime[b];
        });

        vector<int>  select(n);
        select[0] = 0;
        for(int i = 1; i < n; ++i){
            int start = startTime[id[i]];

            int left = 0, right = n;
            while(left < right){
                int mid = (left + right) / 2;
                if(endTime[id[mid]] <= start)
                    left = mid + 1;
                else 
                    right = mid;
            }
            select[i] = left - 1;
        }

        vector<int> dp(n, 0);
        for(int i = 1; i < n; i += 1){
            dp[i] = max(dp[i - 1], dp[select[i]] + profit[id[i]]);
        }
        return dp[n - 1];
    }
};
