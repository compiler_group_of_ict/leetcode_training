class Solution {
public:
    int lengthOfLongestSubstringTwoDistinct(string s) {
        int n = s.size();

        int i = 0;
        char ch1 = s[0];
        int start1 = 0;
        while(i < n && s[i] == ch1){ ++i; }


        int result = i - start1;
        while(i < n){
            char ch2 = s[i];
            int start2 = i;
            while(i < n && s[i] == ch2){++i;}
            result = std::max(result, i - start2);

            int next = i;
            while(i < n && (s[i] == ch1 || s[i] == ch2)){++i;}
            result = std::max(result, i - start1);

            i = next;
            ch1 = ch2;
            start1 = start2;
        }
        return result;
    }
};
