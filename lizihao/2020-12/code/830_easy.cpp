class Solution {
public:
    vector<vector<int>> largeGroupPositions(string s) {
        int i = 0, j = 0;
        int n = s.size();

        vector<vector<int>> result;
        while(j < n){
            if(s[i] == s[j]){
                ++j;
            } else {
                if(j - i >= 3){
                    result.push_back(vector<int>{i, j - 1});
                }
                i = j;
            }
        }
        if(j - i >= 3){
            result.push_back(vector<int>{i, j - 1});
        }

        return std::move(result);
    }
};
