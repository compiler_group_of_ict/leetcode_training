class Solution {
public:
    vector<int> result;
    bool stop;

    bool isFib(vector<int> & buf, long long num){
        auto n = buf.size();
        if(n < 2)
            return true;
        long long op1 = buf[n-1];
        long long op2 = buf[n-2];
        auto sum = op1 + op2;
        return (sum <= INT_MAX) && (sum == num);
    }

    void backtracking(const string &S, int i, int n, vector<int> & buf){
        if(i == n && buf.size() >= 3){
            stop = true;
            result.insert(result.end(), buf.begin(), buf.end());
            return;
        }

        if(stop)
            return;

        if(S[i] == '0'){
            if(isFib(buf, 0)){
                buf.push_back(0);
                backtracking(S, i + 1, n, buf);
                buf.pop_back();
            }
        }
        else{
            long long num = 0;
            for(int t = i; t < n; ++t){
                num *= 10;
                if(num > INT_MAX) break;
                num +=  S.at(t) - '0';
                if(num > INT_MAX) break;

                if(!isFib(buf, num))
                    continue;

                buf.push_back(num);
                backtracking(S, t + 1, n, buf);
                buf.pop_back();
            }
        }
    }
    
    vector<int> splitIntoFibonacci(string S) {
        if(S.empty())
            return vector<int>();

        vector<int> buf;
        stop = false;
        result.clear();
        backtracking(S, 0, S.size(), buf);
        return std::move(result);
    }
};

