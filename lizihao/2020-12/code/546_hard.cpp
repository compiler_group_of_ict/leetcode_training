class Solution {
public:
    int dp[100][100][100];
    inline int square(int x){
        return x * x;
    }

    int iter(vector<int>& boxes, int i, int j, int followed){
        if(i > j) return 0;
        if(dp[i][j][followed] != 0)
            return dp[i][j][followed];

        // Handle Case: length >= 1 && (dp[i][j][followed] == 0)
        
        // Gather same boxes in the tail
        while(j > i && boxes[j] == boxes[j - 1]){
            --j;
            ++followed;
        }
        
        // Stratage-1: directly remove all same boxes in the tail 
        int result = iter(boxes, i, j - 1, 0) + square(followed + 1);

        for(int idx = i; idx < j; ++idx){
            if(boxes[idx] == boxes[j]){
                // Stratage-2: remove middle boxes to get more same boxes
                int eat_between = iter(boxes, idx + 1, j - 1, 0);
                int eat_remained = iter(boxes, i, idx, followed + 1);
                result = std::max(result, eat_between + eat_remained);
            }
        }
        dp[i][j][followed] = result;
        return result;
    }
      
    int removeBoxes(vector<int>& boxes) {
        memset(dp, 1000000 * sizeof(int), 0);
        return iter(boxes, 0, boxes.size() - 1, 0);
    }
};
