class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        if(k == 1)
            return nums;
        
        auto n = nums.size();

        vector<int> prefix(n);
        vector<int> suffix(n + 1);
        for(int i = 0; i < n; ++i){
            prefix[i] = (i % k == 0) ? nums[i] : std::max(prefix[i - 1], nums[i]);
        }
        
        suffix[n] = 0;
        for(int i = n - 1; i >= 0; --i){
            suffix[i] = (i % k == 0) ? nums[i] : std::max(suffix[i + 1], nums[i]);
        }

        vector<int> result;
        int N = n - k;
        for(int i = 0; i <= N; ++i){
            result.push_back(std::max(suffix[i], prefix[i + k - 1]));
        }
        
        return std::move(result);
    }
};
