class Solution {
public:
    int linearFind(vector<vector<int>>& intervals, vector<int>& newInterval, int n){
        int idx = 0;
        for(idx = 0; idx < n && newInterval[0] > intervals[idx][1]; ++idx){
        }
        return idx;
    }

    int binaryFind(vector<vector<int>>& intervals, vector<int>& newInterval, int n){
        int left = 0, right = n;
        while(left < right){
            int mid = (left + right)/2;
            if(newInterval[0] > intervals[mid][1])
                left = mid + 1;
            else
                right = mid;
        }
        return left;
    }

    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        int n = intervals.size();
        int idx = binaryFind(intervals, newInterval, n);
        cout << idx;

        if(idx == n)
            intervals.push_back(newInterval);
        else if(newInterval[0] <= intervals[idx][1] && newInterval[1] >= intervals[idx][0]){
            // �н���
            
            int t = idx + 1;
            while(t < n && newInterval[1] >= intervals[t][0])
                ++t;

            intervals[idx][0] = min(intervals[idx][0], newInterval[0]);
            intervals[idx][1] = max(intervals[t - 1][1], newInterval[1]);
            intervals.erase(intervals.begin() + idx + 1, intervals.begin() + t);
        } else {
            intervals.insert(intervals.begin() + idx, newInterval);
        }
        return intervals;
    }
};
