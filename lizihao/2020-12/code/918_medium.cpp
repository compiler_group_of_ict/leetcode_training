class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int sum = 0;
        int maxNum = INT_MIN;
        for(const auto & x : A){
            sum += x;
            maxNum = std::max(maxNum, x);
        }
        if(maxNum <= 0)
            return maxNum;

        vector<int> B{A};
        int n  = A.size();
        
        int result1 = INT_MIN;
        for(auto i = 1; i < n; ++i){
            B[i] += std::max(0, B[i - 1]);
            result1 = std::max(result1, B[i]);
        }

        int result2 = INT_MAX;
        for(auto i = 1; i < n; ++i){
            A[i] += std::min(0, A[i - 1]);
            result2 = std::min(result2, A[i]);
        }

        return std::max(result1, sum - result2);

    }
};
