/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int iter(TreeNode * root, int & total){
        int val = root->val;
        if(root->left) val += iter(root->left, total);
        if(root->right) val += iter(root->right, total);
        int diff = val - 1;
        total += std::abs(diff);
        return diff;
        
    }

    int distributeCoins(TreeNode* root) {
        if(!root) return 0;
        int total = 0;
        iter(root, total);
        return total;
    }
};
