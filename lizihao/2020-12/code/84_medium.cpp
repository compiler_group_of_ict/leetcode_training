/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
    ListNode* dummyLeft;
    ListNode* dummyRight;
public:
    Solution(){
        dummyLeft = new ListNode(0);
        dummyRight = new ListNode(0);
    }

    ~Solution(){
        delete dummyLeft;
        delete dummyRight;
    }

    ListNode* partition(ListNode* head, int x) {
        ListNode* leftTail = dummyLeft;
        ListNode* rightTail = dummyRight;

        while(head){
            if(head->val < x){
                leftTail->next = head;
                leftTail = head;
            } else {
                rightTail->next = head;
                rightTail = head;
            }
            head = head->next;
        }

        leftTail->next = dummyRight->next;
        rightTail->next = nullptr;
        return dummyLeft->next;
    }
};
