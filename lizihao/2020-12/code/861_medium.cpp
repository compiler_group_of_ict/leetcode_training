class Solution {
public:
    inline void flipRow(vector<int>& row){
        for(auto & bit: row){
            bit = (bit == 0) ? 1 : 0;
        }
    }

    inline void flipCol(vector<vector<int>>& A, int col, int M){
        for(int i = 0; i < M; ++i){
            A[i][col] = (A[i][col] == 0) ? 1 : 0;
        }
    }

    inline int toInt(const vector<int>& row){
        int result = 0;
        for(const auto & bit: row){
            result <<= 1;
            result += bit;
        }
        return result;
    }

    inline 
    int countCol(const vector<vector<int>>& A, int col){
        int result = 0;
        for(const auto & row : A){
            result += row[col];
        }
        return result;
    }

    int matrixScore(vector<vector<int>>& A) {
        int M = A.size();
        int N = A.front().size();
        
        // handle rows
        for(auto & row : A){
            if(row.front() == 0)
                flipRow(row);
        }

        // handle col
        for(int c = 1; c < N; ++c){
            int count = countCol(A, c);
            if(count * 2 < M){
                flipCol(A, c, M);
            }
        }

        int result = 0;
        for(auto & row : A){
            result += toInt(row);
        }
        return result;
    }
};

