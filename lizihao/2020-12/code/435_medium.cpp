// 来自于贪心算法典型题： 区间调度
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        int n = intervals.size();
        if(n <= 1)
            return 0;
        
        sort(intervals.begin(), intervals.end(), [](const vector<int>& range1, const vector<int>& range2){
            return range1[1] < range2[1];
        });
        
        int no_overlapped = 1;
        int last_right = intervals[0][1];
        for(int i = 1; i < n; ++i){
            if(last_right <= intervals[i][0]){
                ++no_overlapped;
                last_right = intervals[i][1];
            }
        }

        return n - no_overlapped;
    }
};
