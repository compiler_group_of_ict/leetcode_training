class Solution {
public:
    bool canPlaceFlowers(vector<int>& flowerbed, int n) {
        int N = flowerbed.size();
        if(N == 1){
            n -= 1 - flowerbed[0];
            return n <= 0;
        }
        int lastPlacedPos = -2;
        for(int i = 0; i < N; ++i){
            if(flowerbed[i] == 1){
                int diff = i - lastPlacedPos;
                if(diff >= 4){
                    n -= diff/2 - 1;
                }
                lastPlacedPos = i;
            }
        }
        n -= (N + 1 - lastPlacedPos)/2 - 1;
        return (n <= 0);
    }
};
