class Solution {
public:
// eeceba
    inline char leftMostEndingChar(const unordered_map<char, int> & lastOccur){
        char ch = -1;
        int leftest = INT_MAX;
        for(const auto &[tmp, idx] : lastOccur){
            if(idx < leftest){
                leftest = idx;
                ch = tmp;
            }
        }
        return ch;
    }

    int lengthOfLongestSubstringKDistinct(string s, int k) {
        int n = s.size();
        if(n == 0 || k == 0)
            return 0;
        
        int result = 0;
        unordered_map<char, int> lastOccur;

        int i = 0, j = 0;
        while(j < n){
            lastOccur[s[j]] = j;
            if(lastOccur.size() > k){
                result = max(result, j - i);

                char victim = leftMostEndingChar(lastOccur);
                i = lastOccur[victim] + 1;
                lastOccur.erase(victim);
            } else{
                ++j;
            }
        }
        result = max(result, j - i);

        return result;
    }
};

