class Solution {
public:
    bool feasable(vector<string>& matrix, int row, int col){
        for(int i = 0; i < row; ++i){
            if(matrix[i][col] == 'Q'){
                return false;
            }
        }

        for(int x = row, y = col; x >= 0 && y >= 0; --x, --y){
            if(matrix[x][y] == 'Q'){
                return false;
            }
        }
        
        int n = matrix.size();
        for(int x = row, y = col; x >= 0 && y < n; --x, ++y){
            if(matrix[x][y] == 'Q'){
                return false;
            }
        }
        return true;
    }

    void backtracking(vector<vector<string>>& result, vector<string>& matrix, int row, int n){
        // std::assert(row >= 0 && row <= n && total >= 0 && total <= n && n >= 0);

        if(row == n){
            result.push_back(matrix);
            return;
        }


        for(int col = 0; col < n; ++col){
            if(false == feasable(matrix, row, col)){
                continue;
            }
            matrix[row][col] = 'Q';
            backtracking(result, matrix, row + 1, n);
            matrix[row][col] = '.';
        }
    }

    vector<vector<string>> solveNQueens(int n) {
        vector<vector<string>> result;
        
        vector<string> matrix(n);
        for(int i = 0; i < n; ++i){
            matrix[i].resize(n, '.');
        }

        backtracking(result, matrix, 0, n);
        
        return std::move(result);
    }
};
