/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int evalHeight(TreeNode* root, bool & isBalancedTree){
        if(root == nullptr || !isBalancedTree)
            return 0;
        else{
            int left = evalHeight(root->left, isBalancedTree);
            int right = evalHeight(root->right, isBalancedTree);
            if(std::abs(left - right) > 1){
                isBalancedTree = false;
            }
            return std::max(left, right) + 1;
        }
    }

    bool isBalanced(TreeNode* root) {
        bool isBalancedTree = true;
        evalHeight(root, isBalancedTree);
        return isBalancedTree;
    }
};
