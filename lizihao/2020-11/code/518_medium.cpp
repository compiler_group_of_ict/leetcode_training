class Solution {
public:
    int change(int amount, vector<int>& coins) {
        if(amount == 0)
            return 1;
        
        if(coins.empty())
            return 0;
    
        
        vector<int> amount2result(amount + 1, 0);
        amount2result[0] = 1;
        for(auto && coin : coins){
            for(int i = coin; i <= amount; ++i){
                amount2result[i] += amount2result[i - coin];
            }
        }
        return amount2result[amount];
    }
};
