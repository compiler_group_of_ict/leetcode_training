class Solution {
public:
    int numWays(int n) {
        if(n <= 1)
            return 1;
        
        int x1 = 1;
        int x2 = 1;
        for(int i = 2; i <= n; ++i){
            x1 = (x1 + x2) % 1000000007;
            std::swap(x1, x2);
        }
        return x2;
    }
}; 
