class Solution {
public:
    int maxValue(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();

        int step =  m + n - 2;
        for(int t = 1; t < m; t++){
            for(auto i = t, j = 0; i >= 0 && j < n; --i, ++j){
                int v = std::max((i - 1 >= 0) ? grid[i - 1][j] : 0,
                                 (j - 1 >= 0) ? grid[i][j - 1] : 0);
                grid[i][j] += v;
            }
        }

        for(int t = m; t <= step; t++){
            for(auto i = m - 1, j = t - m + 1; i >= 0 && j < n; --i, ++j){
                int v = std::max((i - 1 >= 0) ? grid[i - 1][j] : 0,
                                 (j - 1 >= 0) ? grid[i][j - 1] : 0);
                grid[i][j] += v;
            }
        }


        return grid.back().back();
    }
};
