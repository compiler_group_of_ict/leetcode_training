import java.util.ArrayList;
import java.util.List;

class Solution {
    public List<String> generatePossibleNextMoves(String s) {
        List<String> list = new ArrayList<>();
        if (s == null || s.length() == 0) return list;
        for (int i = 0; i < s.length(); i++) {
            if (i != s.length() - 1 && s.charAt(i) == '+' && s.charAt(i) == s.charAt(i + 1)) {
                StringBuilder sb = new StringBuilder(s);
                sb.replace(i, i + 2, "--");
                list.add(sb.toString());
            }
        }
        return list;
    }
}

