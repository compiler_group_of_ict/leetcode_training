class Solution {
public:
    bool feasable(vector<vector<bool>>& matrix, int row, int col){
        for(int i = 0; i < row; ++i){
            if(matrix[i][col]){
                return false;
            }
        }

        for(int x = row, y = col; x >= 0 && y >= 0; --x, --y){
            if(matrix[x][y]){
                return false;
            }
        }
        
        int n = matrix.size();
        for(int x = row, y = col; x >= 0 && y < n; --x, ++y){
            if(matrix[x][y]){
                return false;
            }
        }
        return true;
    }

    void backtracking(int & result, vector<vector<bool>>& matrix, int row, int n){
        // std::assert(row >= 0 && row <= n && total >= 0 && total <= n && n >= 0);

        if(row == n){
            ++result;
            return;
        }

        for(int col = 0; col < n; ++col){
            if(false == feasable(matrix, row, col)){
                continue;
            }
            matrix[row][col] = true;
            backtracking(result, matrix, row + 1, n);
            matrix[row][col] = false;
        }
    }
    int totalNQueens(int n) {
        int result = 0;

        vector<vector<bool>> matrix(n);
        for(int i = 0; i < n; ++i){
            matrix[i].resize(n, false);
        }

        backtracking(result, matrix, 0, n);
        
        return result;
    }
};


