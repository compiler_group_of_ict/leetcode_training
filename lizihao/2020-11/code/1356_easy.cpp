	unsigned countBits(unsigned x){
    unsigned result = 0;
    while(x){
        ++result;
        x = x & (x - 1U);
    }
    return result;
} 
class Solution {
public:

    vector<int> sortByBits(vector<int>& arr) {
        vector<int> result(arr.begin(), arr.end());

        std::sort(result.begin(), result.end(), [](int x, int y)->bool{
            auto x_bits = countBits(x);
            auto y_bits = countBits(y);
            return x_bits < y_bits || (x_bits == y_bits && x < y);
        });

        return std::move(result);
    }
};
