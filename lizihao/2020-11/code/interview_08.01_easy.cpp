class Solution {
public:
    int arr[4];
    Solution(){
        arr[0] = 1;
        arr[1] = 1;
        arr[2] = 2;
        arr[3] = 4;
    }
    inline int addMod(int a, int b){
        return (a + b) % 1000000007;
    }
    int waysToStep(int n) {
        if(n <= 3)
            return arr[n];
        for(int i = 4; i <= n; ++i){
            arr[0] = arr[1];
            arr[1] = arr[2];
            arr[2] = arr[3];
            arr[3] = addMod(addMod(arr[0], arr[1]), arr[2]);
        }
        return arr[3];
    }
};
