class Solution {
public:
    int robLine(vector<int>& nums, int i, int j){
        auto n = j - i + 1; // n >= 2    
        int old = nums[i];
        int present = std::max(nums[i], nums[i + 1]);
        int result = present;
        for(auto t = i + 2; t <= j; ++t){
            old = std::max(nums[t] +  old, present);
            std::swap(old, present);
            result = (present > result) ? present : result;
        }
        return result;
    }

    int rob(vector<int>& nums) {
        auto n =  nums.size();
        if(n == 1){
            return nums.back();
        } else if(n == 2){
            return std::max(nums.back(), nums.front());
        } 
        // n >= 3
        return std::max(robLine(nums, 1, n - 1), robLine(nums, 0, n - 2));

    }
};
