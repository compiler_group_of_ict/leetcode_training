class Solution {
   public:
    int ans;

    void dfs(vector<int>& coins, int amount, int count, int index) {
        if (amount == 0 || index < 0) {
            return;
        }

        for (int coinCount = amount / coins[index]; coinCount >= 0; coinCount--) {

            int newAmount = amount - coins[index] * coinCount;
            int newCount = count + coinCount;

            if (newAmount == 0) { // 被整除
                ans = min(ans, newCount);
                return;
            }

            if (newCount + 1 >= ans) { // 剪枝， 当前结果不够好
                return;
            }
            dfs(coins, newAmount, newCount, index - 1);
        }
    }

    int coinChange(vector<int>& coins, int amount) {
        if (amount == 0) {
            return 0;
        }

        if (coins.size() == 0) {
            return -1;
        }

        ans = INT_MAX;
        sort(coins.begin(), coins.end());
        dfs(coins, amount, 0, coins.size() - 1);
        return ans == INT_MAX ? (-1) : ans;
    }
};
