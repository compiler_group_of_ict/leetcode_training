class Solution {
public:
    inline unsigned square(int x){
        return (x*x);
    }

    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        auto n = points.size();
        
        vector<std::pair<float, decltype(n)>> distance;

        for(auto i = 0U; i < n; ++i){
            auto && d = std::sqrt(square(points[i][0]) + square(points[i][1]));
            distance.push_back(std::make_pair(d, i));
        }

        using value_type = typename std::pair<float, decltype(n)>;
        std::sort(distance.begin(), distance.end(), [](const value_type & pr1, const value_type & pr2)->bool{
            return pr1.first < pr2.first;
        });

        vector<vector<int>> result;
        for(auto i = 0; i < K; ++i){
            auto && idx = distance[i].second;
            result.push_back(points[idx]);
        }
        return std::move(result);
    }
};
