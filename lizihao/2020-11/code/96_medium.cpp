class Solution {
public:
    void evalNumTrees(int n, std::vector<int> & result){
        if(result[n] == -1)
        {
            int sum = 0;
            for(auto i = 1; i <= n; ++i){
                auto left = i - 1;
                auto right = n - i;
                evalNumTrees(left, result);
                evalNumTrees(right, result);
                sum += result[left] * result[right];
            }
            result[n] = sum;
        }
    }
    int numTrees(int n) {
        if(n <= 1)
            return 1;
        else{
            std::vector<int> result(n + 1, -1);
            result[0] = 1;
            result[1] = 1;
            evalNumTrees(n, result);
            return result[n];
        }
    }
};
