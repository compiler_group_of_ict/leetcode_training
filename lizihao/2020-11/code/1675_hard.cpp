class Solution {
public:
    int minimumDeviation(vector<int>& nums) {
        vector<pair<int,int>> v;
        int n = nums.size();
        for(int i = 0;i < n;++i){
            while(nums[i] % 2 == 0){
                v.emplace_back(nums[i],i); 
                nums[i]/=2;
            }
            v.emplace_back(nums[i],i);
            v.emplace_back(nums[i] * 2,i);
        }
        sort(v.begin(),v.end());    
        int vis[100005] = {0};
        int l = 0,r = 0;
        int cnt = 0;
        int res = INT_MAX;
        while(r <  v.size()){
            while(cnt == n){    
                res = min(res,v[r-1].first - v[l].first);  
                vis[v[l].second]--;
                if(vis[v[l].second] == 0) cnt--;
                l++;
            }
            vis[v[r].second]++;
            if(vis[v[r].second] == 1) cnt++;
            r++;
        }
        return res;
    }
};
