class dsu
{
public:
    vector<int> f;
    dsu(int n)
    {
        f.resize(n);
        for(int i = 0; i < n; ++i)
            f[i] = i;
    }
    bool merge(int a, int b)
    {
        int fa = find(a);
        int fb = find(b);
        if(fa != fb)
        {
            f[fa] = fb;
            return true;
        }
        return false;
    }
    int find(int a)
    {
        int origin = a;
        while(a != f[a])
            a = f[a];
        return f[origin] = a;
    }
};
class Solution {
public:
    vector<int> numIslands2(int m, int n, vector<vector<int>>& positions) {
    	int N = m*n, pos, x, y;
    	vector<vector<int>> grid(m,vector<int>(n,0));
    	dsu u(N);
    	vector<int> ans(positions.size());
    	vector<vector<int>> dir = {{1,0},{0,1},{0,-1},{-1,0}};
        unordered_set<int> s;
    	for(int i = 0, k; i < positions.size(); ++i)
    	{
            ans[i] = (i>0 ? ans[i-1] : 0 )+1;
            grid[positions[i][0]][positions[i][1]] = 1;
            pos = positions[i][0]*n+positions[i][1];
            if(s.count(pos))
            {
                ans[i]--;
                continue;
            }
            s.insert(pos);
    		for(k = 0; k < 4; ++k)
    		{
    			x = positions[i][0] + dir[k][0];
                y = positions[i][1] + dir[k][1];
    			if(x>=0 && x<m && y>=0 && y<n && grid[x][y]==1)
                {
                    if(u.merge(pos, x*n+y))
                        ans[i]--;
                }
    		}
    	}
    	return ans;
    }
};
