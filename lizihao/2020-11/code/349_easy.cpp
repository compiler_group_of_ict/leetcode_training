class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        std::sort(nums1.begin(), nums1.end());
        std::sort(nums2.begin(), nums2.end());
        auto it1 = std::unique(nums1.begin(), nums1.end());
        nums1.resize(std::distance(nums1.begin(), it1));

        auto it2 = std::unique(nums2.begin(), nums2.end());
        nums2.resize(std::distance(nums2.begin(), it2));

        vector<int> result;

        int i = 0, j = 0;
        int n1 = nums1.size(), n2 = nums2.size();
        while(i < n1 && j < n2){
            if(nums1[i] == nums2[j]){
                result.push_back(nums1[i]);
                ++i;
                ++j;
            } else if(nums1[i] < nums2[j]){
                ++i;
            } else{
                ++j;
            }
        }
        return std::move(result);
    }
};

