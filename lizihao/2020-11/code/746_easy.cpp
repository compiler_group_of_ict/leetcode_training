class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int n = cost.size();

        int minCostInStair1 = 0;
        int minCostInStair2 = 0;
        for(int i = 2; i <= n; ++i){
            minCostInStair1 = std::min(minCostInStair2 + cost[i - 1], minCostInStair1 + cost[i - 2]);
            std::swap(minCostInStair1, minCostInStair2);
        }
        return minCostInStair2;
    }
};
