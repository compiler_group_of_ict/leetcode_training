class ZigzagIterator {
public:
    ZigzagIterator(vector<int>& v1, vector<int>& v2) {
        this->v1 = v1;
        this->v2 = v2;
        iter1 = this->v1.begin();
        iter2 = this->v2.begin();
        index = 0;
    }

    int next() {
        index++;
        if(index % 2 == 0){
            if(iter2 != v2.end()){
                int ans = *iter2;
                iter2++;
                return ans;
            }
            else{
                int ans = *iter1;
                iter1++;
                return ans;
            }
        }
        else{
            if(iter1 != v1.end()){
                int ans = *iter1;
                iter1++;
                return ans;
            }
            else{
                int ans = *iter2;
                iter2++;
                return ans;
            }
        }
        
    }

    bool hasNext() {
        return iter1 != v1.end() || iter2 != v2.end();
    }

    vector<int> v1, v2;
    vector<int>::iterator iter1, iter2;
    int index;
};

/**
 * Your ZigzagIterator object will be instantiated and called as such:
 * ZigzagIterator i(v1, v2);
 * while (i.hasNext()) cout << i.next();
 */
