class Solution {
public:
    int minimumEffort(vector<vector<int>>& tasks) {
        int res = 0, remain = 0;
        for(auto task: tasks) res += task[0];
        remain = res;
        
        sort(tasks.begin(), tasks.end(), [](vector<int> & v, vector<int> &w) {return v[1] - v[0] < w[1] - w[0];});
        
        for(int i = tasks.size() - 1; i >= 0; i--) {
            if(remain >= tasks[i][1] && remain >= tasks[i][0]) remain -= tasks[i][0];
            else if(remain < tasks[i][1] && remain < tasks[i][0]) {
                res += max(tasks[i][1] - remain, tasks[i][0] - remain);
                remain += max(tasks[i][1] - remain, tasks[i][0] - remain);
                remain -= tasks[i][0];
            }
            else if(remain < tasks[i][1] && remain >= tasks[i][0]) {
                res += tasks[i][1] - remain;
                remain += tasks[i][1] - remain;
                remain -= tasks[i][0];
            }
            else if(remain >= tasks[i][1] && remain < tasks[i][0]) {
                res += tasks[i][0] - remain;
                remain += tasks[i][0] - remain;
                remain -= tasks[i][0];
            }
        }
        return res;
    }
};

