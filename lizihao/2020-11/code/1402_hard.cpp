class Solution {
public:
    int maxSatisfaction(vector<int>& satisfaction) {
        std::sort(satisfaction.begin(), satisfaction.end());
        
        int n = satisfaction.size();
        vector<vector<int>> dp(n + 1); // dp[i][t]
        for(int i = 0; i <= n; ++i){
            dp[i].resize(i + 1, 0);
        }

        for(auto i = 1; i <= n; ++i){
            // i-th dish
            for(auto t = 1; t < i; ++t){
                dp[i][t] = std::max(dp[i - 1][t], dp[i - 1][t - 1] + t * satisfaction[i - 1]);
            }

            dp[i][i] = dp[i - 1][i - 1] + i * satisfaction[i - 1]; // t == i
        }

        int result = 0;
        for(auto t = 1; t <= n; ++t){
            result = std::max(result, dp[n][t]);
        }
        return result;
    }
};
