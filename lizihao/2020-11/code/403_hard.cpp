class Solution {
public:
    bool dfs(int current_stone, int dst_stone, int last_step, std::unordered_map<int, std::unordered_map<int, bool>> & result){
        if(current_stone == dst_stone){
            return true;
        }else if(result.find(current_stone) == result.end()){
            // no such stone
            return false;
        } else if(result[current_stone].find(last_step) != result[current_stone].end()){
            // have already known the result
            return result[current_stone][last_step];
        } else {
            // std::cout << "eval: stone " << current_stone << ", last_step " << last_step << endl;
            auto min_step = std::max(1, last_step - 1);
            for(auto step = last_step + 1; step >= min_step; --step){
                if(dfs(current_stone + step, dst_stone, step, result) == true){
                    result[current_stone][last_step] = true;
                    return true;
                }
            }
            result[current_stone][last_step] = false;
            return false;
        }
    }

    bool canCross(vector<int>& stones) {
        if(stones.back() > 603351){

            return false;
        }

        std::unordered_map<int, std::unordered_map<int, bool>> result;
        for(auto && stone : stones){
            result.insert(std::make_pair(stone, std::unordered_map<int, bool>()));
        }
        
        return dfs(stones.front(), stones.back(), 0, result);
    }
};
