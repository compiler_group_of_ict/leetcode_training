class Solution {
public:
    int rob(vector<int>& nums) {
        auto n = nums.size();
        if(n == 0)
            return 0;
        
        if(n == 1)
            return nums.front();
        
        int old = nums[0];
        int present = std::max(nums[0], nums[1]);
        int result = present;
        for(auto i = 2; i < n; ++i){
            old = std::max(nums[i] +  old, present);
            std::swap(old, present);
            result = (present > result) ? present : result;
        }
        return result;
    }
};
