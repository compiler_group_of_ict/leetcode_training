class Solution {
public:
    void dfs(int v, int n, vector<int> & result){
        // std::assert(v <= n);

        result.push_back(v);
        v *= 10;
        for(int i = 0; i < 10 && v + i <= n; ++i){
            dfs(v + i, n, result);
        }
    }

    vector<int> lexicalOrder(int n) {
        vector<int> result;
        for(int i = 1; i < 10 && i <= n; ++i){
            dfs(i, n, result);
        }
        return result;
    }
};
