class Solution {
public:

    int maximumSum(vector<int>& arr) {
        int result = arr[0];
        int pre_with_no_remove = result;
        int pre_with_one_remove = result;

        int n = arr.size();

        for(auto i = 1; i < n; ++i){
            auto&& data = arr[i];
            pre_with_one_remove = std::max(pre_with_no_remove, 
						pre_with_one_remove + data);
		    pre_with_no_remove = std::max(data, data + pre_with_no_remove);
		    result = std::max(result, std::max(pre_with_one_remove, pre_with_no_remove));
        }
        return result;
    }
};

