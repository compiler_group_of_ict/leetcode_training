class Solution {
public:
    enum {kMaxLen = 500};
    int dp[kMaxLen + 1][kMaxLen + 1];

    int min3(int x, int y, int z){
        return std::min(std::min(x, y), z);
    }

    int minDistance(string word1, string word2) {
        int n1 = word1.size();
        int n2 = word2.size();

        for(int i = 0; i <= n1; ++i)
            dp[i][0] = i;
        
        for(int i = 0; i <= n2; ++i)
            dp[0][i] = i;
        
        for(int e1 = 1; e1 <= n1; ++e1){
            for(int e2 = 1; e2 <= n2; ++e2){
                 const int l1r1 = dp[e1 - 1][e2 -1];
                if(word1[e1 - 1] == word2[e2 - 1]){
                    dp[e1][e2] = l1r1;
                } else {
                    
                    dp[e1][e2] = std::min(std::min(dp[e1 - 1][e2], dp[e1][e2 -1]), l1r1) + 1;
                }
            }
        }

        return dp[n1][n2];
    }
};
