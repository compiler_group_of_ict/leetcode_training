/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    std::pair<bool, int> evalBalancingAndHeight(TreeNode* root){
        if(root == nullptr){
            return std::make_pair(true, 0);
        } else {
            auto [left_flag, left_height] = evalBalancingAndHeight(root->left);
            auto [right_flag, right_height] = evalBalancingAndHeight(root->right);
            auto flag = left_flag && right_flag && std::abs(left_height - right_height) <= 1;
            return std::make_pair(flag, std::max(left_height, right_height) + 1);
        }
    }
    bool isBalanced(TreeNode* root) {
        return evalBalancingAndHeight(root).first;
    }
};
