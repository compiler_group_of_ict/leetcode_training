class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        int count_5 = 0;
        int count_10 = 0;
        for(auto && bill : bills){
            switch(bill){
            case 5:
                ++count_5;
                break;
            case 10:
                if(count_5 == 0)
                    return false;
                ++count_10;
                --count_5;
                break;
            case 20:
                if(count_10 != 0){
                    --count_10;
                    if(count_5 != 0){
                        --count_5;
                    } else {
                        return false;
                    }
                } else{
                    if(count_5 < 3){
                        return false;
                    } else {
                        count_5 -= 3;
                    }
                }
            }
        }
        return true;
    }
};
